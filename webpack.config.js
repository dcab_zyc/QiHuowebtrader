switch (process.env.NODE_ENV) {
  case 'prod':
    module.exports = require('./webpack/webpack.prod');
    break;
  case 'dev':
    module.exports = require('./webpack/webpack.dev');
    break;
  case 'test':
    module.exports = require('./webpack/webpack.test');
    break;
}