import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Observable";

import { AppComonent } from 'app/app.component';
// import { HeaderComponent } from 'components/Header/Header.component';
// import { FooterComponent } from 'components/Footer/Footer.component';
import { ROUTES } from 'app/app.router';


import { AuthGuard } from 'service/AuthGuard';
import { AuthService } from 'service/AuthService';
import { HttpService } from 'service/HttpService';
import { ApiRequest } from 'service/ApiRequest';
import { ApiCanLoad } from 'service/ApiCanLoad';
import { LoginService } from 'views/Login/Login.service';

@NgModule({
  bootstrap: [AppComonent],　　　　　　//引导组件，程序的入口
  declarations: [               //申明模块内部成员
    AppComonent,
    // HeaderComponent,
    // FooterComponent
  ],
  imports: [                  //导入Angular　Module,被导入Module可以在本模块范围内使用
    BrowserModule,
    RouterModule.forRoot(ROUTES, { useHash: true }),
    CommonModule,
    HttpModule
  ],
  exports: [                 //内部成员暴露给外部使用
    RouterModule
  ],
  providers: [               //根级别的Service
    AuthGuard,
    AuthService,
    ApiRequest,
    HttpService,
    // {
    //   provide: ApiRequest,
    //   deps: [HttpService]}
    ,
    ApiCanLoad,
    LoginService,
  ]
})
export class AppModule {
  constructor() { }
}
