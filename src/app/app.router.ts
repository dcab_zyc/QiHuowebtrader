// import { Routes } from '@angular/router';

// import { AppComonent } from './app.component';
// import { ApiCanLoad } from 'service/ApiCanLoad';
// import { AuthGuard } from "service/AuthGuard";

// export const ROUTES: Routes = [
//   {path: '', redirectTo: 'index', pathMatch: 'full'},
//   {path: 'index', canActivateChild: [ApiCanLoad], children: [
//     {path: '', loadChildren: "views/Main/Main.module#MainModule"},
//     {path: 'login', loadChildren: "views/Login/Login.module#LoginModule"},
//     {path: 'news', loadChildren: "views/News/News.module#NewsModule"},
//     {path: 'user', loadChildren: "views/User/User.module#UserModule", canActivate:[AuthGuard]},
//     {path: 'calendar', loadChildren: "views/Calendar/Calendar.module#CalendarModule"},
//     {path: 'market', loadChildren: "views/Market/Market.module#MarketModule"}
//   ]},
//   { path: 'trade', loadChildren: 'views/WebTrader_Trade/WebTrader_Trade.module#WebTraderTradeModule' },
// ];

import { Routes } from '@angular/router';

import { AppComonent } from './app.component';
import { ApiCanLoad } from 'service/ApiCanLoad';
import { AuthGuard } from "service/AuthGuard";

export const ROUTES: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'trade', canActivateChild: [ApiCanLoad], loadChildren: 'views/WebTrader_Trade/WebTrader_Trade.module#WebTraderTradeModule' },
  { path: 'index', canActivateChild: [ApiCanLoad], loadChildren: 'views/WebTrader/WebTrader.module#WebTraderModule' },
];
