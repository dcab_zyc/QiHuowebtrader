import { Component } from '@angular/core';

@Component({
  selector: 'app',
  styleUrls: [],
  template: `
    <router-outlet></router-outlet>`,
  // template: `
  // <div class="app">
  //   <header-menu></header-menu>
  //   <div class="content">
  //     <router-outlet></router-outlet>
  //   </div>
  //   <footer-bottom></footer-bottom>
  // </div>
  // `,
})

export class AppComonent {
  constructor() {
  }

  ngOnInit() {

  }
  ngOnChanges() {
  }
  ngDoCheck() {

  }
  ngAfterContentInit() {

  }
  ngAfterContentChecked() {

  }
  ngAfterViewInit() {

  }
  ngAfterViewChecked() {

  }
  ngOnDestroy() {

  }
}
