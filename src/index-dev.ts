import 'angular2-ie9-shims/shims_for_IE.dev.js';
import { Observable } from "rxjs/Observable";

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);