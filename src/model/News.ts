export class News {
  Editor: string;
  Summary: string;
  Content: string;
  Source: string;
  Category: any;
  Tags: Array<string>;
  Id: string;
  Time: number;
  Title: string;
  IconUrl: string;
  EditTime: number;
}