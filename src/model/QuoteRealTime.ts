export class QuoteRealTime {
    Symbol:string;
    Time:number;
    Volume:number;
    YesterdayClose: number;
    DayOpen: number;
    DayHigh:number;
    DayLow: number;
    CurrentPrice:number;
    BuyPrice:number;
    SellPrice:number;
};