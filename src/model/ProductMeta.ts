import { IQuoteBasic } from 'interface/IQuoteBasic';

export class ProductMeta {
    Symbol: string;
    SymbolDisplay: string;
    Category: string;
    CategoryDisplay: string;
};