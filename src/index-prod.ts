import 'angular2-ie9-shims/shims_for_IE.dev.js';
import { Observable } from "rxjs/Observable";
import { enableProdMode } from '@angular/core';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';


enableProdMode();
platformBrowserDynamic().bootstrapModule(AppModule);