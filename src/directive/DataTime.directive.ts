import { Directive, ElementRef, Input, Renderer } from '@angular/core';

@Directive({ selector: '[datetime]' })
export class DataTimeDirective{
    @Input() datetime:string;

    constructor(private el: ElementRef, private renderer: Renderer){

    }

    ngOnInit() {
      this.renderer.setElementAttribute(this.el.nativeElement, 'data-time', this.datetime);
    }
}