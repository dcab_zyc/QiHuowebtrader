import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './Header.component';


@NgModule({
  declarations: [
    HeaderComponent,

  ],
  imports:[
    CommonModule,
    RouterModule
  ],
  exports: [

  ],
  providers: [
  ],
})

export class HeaderModule {
  constructor() {
    
  }
}