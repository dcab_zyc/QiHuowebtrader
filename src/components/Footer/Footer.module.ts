import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './Footer.component';


@NgModule({
  declarations: [
    FooterComponent,

  ],
  imports:[
    CommonModule,
    RouterModule
  ],
  exports: [
    FooterComponent
  ],
  providers: [
  ],
})

export class FooterModule {
  constructor() {
    
  }
}