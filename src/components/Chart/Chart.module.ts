import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EchartsNg2Module } from 'echarts-ng2';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
//import { SignalRModule, SignalRConfiguration } from 'ng2-signalr';
//import 'signalr/jquery.signalR.js';

import { ChartComponent } from './Chart.component';

import { ChartService } from './Chart.service';
import { CandleService } from 'service/CandleService';
import { StatService } from 'service/StatService';
import { MAService } from 'service/MAService';
import { BollService } from 'service/BollService';
import { RSIService } from 'service/RSIService';
import { EMAService } from 'service/EMAService';
import { MACDService } from 'service/MACDService';
import { BIASService } from 'service/BIASService';
import { LineService } from 'service/LineService';

import { HttpService } from 'apirequest_yeez/ApiRequest/Common/Services/HTTPService';
import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';

@NgModule({
  declarations: [
    ChartComponent,
  ],
  imports: [
    CommonModule,
    EchartsNg2Module,
    FormsModule,
    RouterModule,
  ],
  exports: [
    ChartComponent,

  ],
  providers: [
    ChartService,
    CandleService,
    StatService,
    MAService,
    BollService,
    RSIService,
    EMAService,
    MACDService,
    BIASService,
    LineService,
    HttpService,
    ApiRequest,
  ],
})

export class ChartModule {
  constructor() {

  }
}