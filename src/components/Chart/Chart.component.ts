import { Component, ViewChild, Input } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { EChartOption, ECharts } from 'echarts-ng2';
import * as _ from 'lodash';
import * as moment from 'moment';
import { QuoteBasic } from 'service/QuoteBasic';

import { StatService } from 'service/StatService';
import { SignalRService } from 'service/SignalRService';
import { ApiRequest } from 'service/ApiRequest';

import { QuoteRealTime } from 'model/QuoteRealTime';

import {
	BASE_OPTION,
	XAXIS,
	YAXIS_BASE
} from 'utils/chart-config';

import {
	HTTP_SUCCESS,
	STAT_RANGE,
	MACD_STAT,
	BOLL_STAT,
	RSI_STAT,
	BIAS_STAT,
	MA_STAT,
	EMA_STAT,

	EMA_COLOR,
	MA_COLOR,
	MACD_COLOR,
	BOLL_COLOR,
	RSI_COLOR,
	BIAS_COLOR,
	CANDLE_COLOR,
	LINE_COLOR
} from 'utils';

import { Http } from '@angular/http';

const LINE = 'line';
const CANDLE = 'candle';

@Component({
	selector: 'chart',
	templateUrl: './Chart.html'
}
)
export class ChartComponent {
	isLoading: boolean = false;

	@Input() status;

	option = BASE_OPTION;
	ranges = STAT_RANGE;
	rangeIndex = 0;

	lineTypes = [{
		show: '蜡烛图',
		icon: 'assets/image/candle.png'
	}, {
		show: '线形图',
		icon: 'assets/image/line.png'
	}];
	lineTypeIndex = 0;

	showStatPanle = false;

	macdStat;
	bollStat;
	rsiStat;
	biasStat;
	maStat;
	emaStat;

	macdColor = MACD_COLOR;
	maColor = MA_COLOR;
	emaColor = EMA_COLOR;
	rsiColor = RSI_COLOR;
	bollColor = BOLL_COLOR;
	biasColor = BIAS_COLOR;
	candleColor = CANDLE_COLOR;
	lineColor = LINE_COLOR;

	@Input('symbol') symbol;
	symbolList = [];

	quoteRealTime = new QuoteRealTime();


	quoteBasic: QuoteBasic;

	@ViewChild('echarts') echarts: ECharts;
	private pagination: PaginationInstance = {
		id: 'market-list',
		itemsPerPage: 10,
		currentPage: 0,
		totalItems: 1000
	};

	signalRService: SignalRService = new SignalRService('forexHub');

	constructor(private chartService: StatService, private apiRequest: ApiRequest, private http: Http) {
		this.chartService.setParams('http://114.55.146.142:2004', '/api/data/GetApiUrl');
	}

	async ngOnInit() {
		await this.chartService.init();

		const interval = this.ranges[0].interval;
		const eTime = moment().valueOf();
		// const sTime = eTime - this.ranges[0].range;
		const sTime = 0;
		let t = setInterval(() => {
			if (this.symbol) {
				clearInterval(t);
				this.loadData(this.symbol, interval, sTime, eTime);
			}
		},10);

		$("body").on(("click"), (e) => {
			this.showStatPanle = false;
			return true;
		});

		$(".dropdown-stat").on("click", (e) => {
			e.stopPropagation();
			e.cancelBubble = false;
			$(".dropdown.open").removeClass("open");
			return;
		});

		// 改变窗口大小、改变图表大小
		window['echarts'] = this.echarts;
		$(window).resize(() => {
			const echarts = window['echarts'];
			if (echarts && echarts.resize) {
				echarts.resize();
			}
		});
	}

	ngOnDestroy() {
		delete window['echarts'];
		this.signalRService.unSubscribe(this.symbol);
	}

	async loadData(symbol, interval, sTime, eTime, maxCount = 100) {
		await this.chartService.getHistoricalQuote(symbol, interval, Math.floor(sTime / 1000), Math.ceil(eTime / 1000), maxCount).then((data) => {
			this.quoteBasic = data;
			console.log(data);
			// this.recacuChart();
		}, (e) => {
			console.log("ChartComponent: loadData", e);
		});

		this.signalRService.registerOn("onNewBasicProductInfoStr", (symbol, data) => {
			const list = data.split("-");
			const close = parseFloat(list[7]);
			const time = parseInt(list[1]);

			this.quoteRealTime.Symbol = list[0];
			this.quoteRealTime.Time = parseInt(list[1]);
			this.quoteRealTime.Volume = parseFloat(list[2]);
			this.quoteRealTime.YesterdayClose = parseFloat(list[3]);
			this.quoteRealTime.DayOpen = parseFloat(list[4]);
			this.quoteRealTime.DayHigh = parseFloat(list[5]);
			this.quoteRealTime.DayLow = parseFloat(list[6]);
			this.quoteRealTime.CurrentPrice = parseFloat(list[7]);
			this.quoteRealTime.BuyPrice = parseFloat(list[8]);
			this.quoteRealTime.SellPrice = parseFloat(list[9]);

			const flag = this.quoteBasic.append(symbol, time * 1000, close);
			if (flag) {
				try {
					this.updatePoint();
				} catch (e) {
					console.log(e);
				}
			}
		});
		this.signalRService.getConnection().done((data: any) => {
			this.signalRService.subscribe(this.symbol);
		}).fail((error: any) => {
			console.log('Could not connect ' + error);
		});
		// // 雅虎测试数据
		// await this.http.get("../../utils/chart-testData.json").map(res => res.json()).subscribe(data => {
		// 		const time = data['Time'].map(item => item * 1000);
		//   	this.quoteBasic = new QuoteBasic('^IXIC', interval, time, data['Open'], data['Low'], data['High'], data['Close']);
		// 		this.recacuChart();
		// }, (err) => {
		// 	console.log(err);
		// });
	}

	onRangeChange(index) {
		if (index === this.rangeIndex) {
			return;
		}

		const interval = this.ranges[index].interval;
		const eTime = moment().valueOf();
		// const sTime = eTime - this.ranges[index].range;
		const sTime = 0;
		this.loadData(this.symbol, interval, sTime, eTime);
		this.rangeIndex = index;

	}

	onLineTypeChange(index) {
		if (this.lineTypeIndex === index) {
			return;
		}

		this.lineTypeIndex = index;

		if (this.lineTypeIndex === 0) {
			this.removeLines("Line");
			this.addLines("Candle");
		} else {
			this.removeLines("Candle");
			this.addLines("Line");
		}
	}

	onStatShow(e) {
		this.showStatPanle = !this.showStatPanle;
	}

	showMACDStat() {
		if (this.macdStat) {
			this.macdStat = null;
			this.removeGrid("MACD");
		} else {
			this.macdStat = _.cloneDeep(MACD_STAT);
			this.addGrid("MACD", []);
		}
	}

	showBollStat() {
		if (this.bollStat) {
			this.bollStat = null;
			this.removeLines("BOLL");
		} else {
			this.bollStat = _.cloneDeep(BOLL_STAT);
			this.addLines("BOLL");
		}
	}

	showRSIStat() {
		if (this.rsiStat) {
			this.rsiStat = null;
			this.removeGrid("RSI");
		} else {
			this.rsiStat = _.cloneDeep(RSI_STAT);
			this.addGrid("RSI", []);
		}
	}

	showBIASStat() {
		if (this.biasStat) {
			this.biasStat = null;
			this.removeGrid("BIAS");
		} else {
			this.biasStat = _.cloneDeep(BIAS_STAT);
			this.addGrid("BIAS", []);
		}
	}

	showMAStat() {
		if (this.maStat) {
			this.maStat = null;
			this.removeLines("MA");
		} else {
			this.maStat = _.cloneDeep(MA_STAT);
			this.addLines("MA");
		}
	}

	showEMAStat() {
		if (this.emaStat) {
			this.emaStat = null;
			this.removeLines("EMA");
		} else {
			this.emaStat = _.cloneDeep(EMA_STAT);
			this.addLines("EMA");
		}
	}

	onBlur(e, cate, type) {
		const value = parseInt(e.target.value);
		let name = null;
		if (!isNaN(value) && this[cate][type].min <= value && value <= this[cate][type].max) {
			this[cate][type].value = value;
			if (cate == "macdStat") {
				name = "MACD";
			} else if (cate == "bollStat") {
				name = "BOLL";
			} else if (cate == "rsiStat") {
				name = "RSI";
			} else if (cate == 'biasStat') {
				name = "BIAS";
			} else if (cate == 'maStat') {
				name = "MA";
			} else if (cate == 'emaStat') {
				name = "EMA";
			}
		} else {
			if (cate == "macdStat") {
				name = "MACD";
				this[cate][type].value = MACD_STAT[type].value;
			} else if (cate == "bollStat") {
				name = "BOLL";
				this.bollStat[type].value = BOLL_STAT[type].value;
			} else if (cate == "rsiStat") {
				name = "RSI";
				this.rsiStat[type].value = RSI_STAT[type].value;
			} else if (cate == 'biasStat') {
				name = "BIAS";
				this.biasStat[type].value = BIAS_STAT[type].value;
			} else if (cate == 'maStat') {
				name = "MA";
				this.maStat[type].value = MA_STAT[type].value;
			} else if (cate == 'emaStat') {
				name = "EMA";
				this.emaStat[type].value = EMA_STAT[type].value;
			}
		}

		name && this.updateLines(name);
	}

	addGrid(name, data) {
		const tempOption = _.cloneDeep(this.option);
		const count = tempOption.grid.length;

		let grid = [];
		let xAxis = [];
		let yAxis = [];
		let series = [];
		let dataZoom = this.echarts.getOption()['dataZoom'];
		dataZoom[0].xAxisIndex = [];

		const tempGrid = tempOption.grid;
		for (let i = 0; i < count; i++) {
			grid.push(Object.assign({}, tempGrid[i], {
				height: i == 0 ? `${(1 - 0.2 - 0.1 * count - 0.05) * 100}%` : '8%',
				top: i == 0 ? '8%' : `${(1 - 0.2 - 0.1 * count + i * 0.1) * 100}%`,
			}));
			dataZoom[0].xAxisIndex.push(i);
		}

		grid.push({
			name: name,
			top: '80%',
			left: '10%',
			right: '8%',
			height: '8%'
		});

		xAxis = _.cloneDeep(tempOption.xAxis);
		xAxis.push(Object.assign({}, XAXIS, {
			data: this.quoteBasic.getTimeString(this.ranges[this.rangeIndex].show),
			gridIndex: count
		}));

		yAxis = _.cloneDeep(tempOption.yAxis);
		yAxis.push(Object.assign({}, YAXIS_BASE, {
			gridIndex: count
		}));

		series = _.cloneDeep(tempOption.series);

		if (name === "MACD") {
			//yAxis[count].min = 0;
			const data = this.chartService.getMACDData(this.quoteBasic, this.macdStat.long.value, this.macdStat.short.value, this.macdStat.m.value, );

			series.push(this.combineLine("DIF", "line", data[0], this.macdColor.dif, count));
			series.push(this.combineLine("DEA", "line", data[1], this.macdColor.dea, count));
			series.push(this.combineLine("MACD", "bar", data[2], this.macdColor.macd, count));
		}

		if (name === "RSI") {
			const rsi1 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi1.value);
			series.push(this.combineLine("RSI1", 'line', rsi1, this.rsiColor.rsi1, count));

			const rsi2 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi2.value);
			series.push(this.combineLine("RSI2", 'line', rsi2, this.rsiColor.rsi2, count));

			const rsi3 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi3.value);
			series.push(this.combineLine("RSI3", 'line', rsi3, this.rsiColor.rsi3, count));

		}

		if (name === "BIAS") {
			const bias1 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias1.value);
			series.push(this.combineLine("BIAS1", 'line', bias1, this.biasColor.bias1, count));

			const bias2 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias2.value);
			series.push(this.combineLine("BIAS2", 'line', bias2, this.biasColor.bias2, count));

			const bias3 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias3.value);
			series.push(this.combineLine("BIAS3", 'line', bias3, this.biasColor.bias3, count));
		}

		dataZoom[0].xAxisIndex.push(count);

		this.echarts.clear();
		this.option = Object.assign(tempOption, {
			grid,
			xAxis,
			yAxis,
			series,
			dataZoom
		});
	}

	removeGrid(name) {
		const tempOption = _.cloneDeep(this.option);

		const index = _.findIndex(tempOption.grid, (item) => item['name'] === name);
		const tempGrid = tempOption.grid;
		tempGrid.splice(index, 1);
		const count = tempGrid.length;

		let grid = [];
		let xAxis = [];
		let yAxis = [];
		let series = [];
		let dataZoom = this.echarts.getOption()['dataZoom'];
		dataZoom[0].xAxisIndex = [];

		for (let i = 0; i < tempGrid.length; i++) {
			grid.push(Object.assign({}, tempGrid[i], {
				height: i == 0 ? `${(1 - 0.3 - 0.1 * count) * 100}%` : '8%',
				top: i == 0 ? '10%' : `${(1 - 0.2 - 0.1 * count + i * 0.1) * 100}%`,
			}));
			dataZoom[0].xAxisIndex.push(i);
		}

		xAxis = _.cloneDeep(tempOption.xAxis);
		xAxis = xAxis.filter(item => item.gridIndex !== index).map((item) => {
			if (item.gridIndex > index) {
				item.gridIndex--;
			}
			return item;
		});

		yAxis = _.cloneDeep(tempOption.yAxis);
		yAxis = yAxis.filter(item => item.gridIndex !== index).map((item) => {
			if (item.gridIndex > index) {
				item.gridIndex--;
			}
			return item;
		});

		series = _.cloneDeep(tempOption.series);


		series = series.filter(item => item.xAxisIndex !== index && item.yAxisIndex !== index).map((item) => {
			if (item.xAxisIndex > index && item.yAxisIndex > index) {
				item.xAxisIndex--;
				item.yAxisIndex--;
			}
			return item;
		});

		this.echarts.clear();

		this.option = Object.assign(tempOption, {
			dataZoom,
			grid,
			xAxis,
			yAxis,
			series
		});
	}

	recacuChart() {
		const tempOption = this.option;

		let xAxis = tempOption['xAxis'];
		let yAxis = tempOption['yAxis'];
		let series = [];
		let grid = tempOption['grid'];

		xAxis = xAxis.map(item => {
			item.data = this.quoteBasic.getTimeString(this.ranges[this.rangeIndex].show);
			return item;
		});

		if (this.lineTypeIndex == 0) {
			series.push(this.combineLine("Candle", "candlestick", this.chartService.getCandleData(this.quoteBasic), [this.candleColor.color, this.candleColor.color0]));
		} else {
			series.push(this.combineLine("Line", "line", this.chartService.getLineData(this.quoteBasic), this.lineColor.line));
		}

		if (this.macdStat) {
			let index = _.findIndex(grid, (item) => item['name'] === 'MACD');
			const data = this.chartService.getMACDData(this.quoteBasic, this.macdStat.long.value, this.macdStat.short.value, this.macdStat.m.value, );
			series.push(this.combineLine("DIF", "line", data[0], this.macdColor.dif, index));
			series.push(this.combineLine("DEA", "line", data[1], this.macdColor.dea, index));
			series.push(this.combineLine("MACD", "bar", data[2], this.macdColor.macd, index));
		}

		if (this.bollStat) {
			const bollData = this.chartService.getBollData(this.quoteBasic, this.bollStat.stand.value, this.bollStat.width.value);
			series.push(this.combineLine("MID", "line", bollData[0], this.bollColor.mid));
			series.push(this.combineLine("UP", "line", bollData[1], this.bollColor.up));
			series.push(this.combineLine("LOW", "line", bollData[2], this.bollColor.low));
		}

		if (this.rsiStat) {
			let index = _.findIndex(grid, (item) => item['name'] === 'RSI');
			const rsi1 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi1.value);
			series.push(this.combineLine("RSI1", 'line', rsi1, this.rsiColor.rsi1, index));

			const rsi2 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi2.value);
			series.push(this.combineLine("RSI2", 'line', rsi2, this.rsiColor.rsi2, index));

			const rsi3 = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi3.value);
			series.push(this.combineLine("RSI3", 'line', rsi3, this.rsiColor.rsi3, index));
		}

		if (this.biasStat) {
			let index = _.findIndex(grid, (item) => item['name'] === 'BIAS');
			const bias1 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias1.value);
			series.push(this.combineLine("BIAS1", 'line', bias1, this.biasColor.bias1, index));

			const bias2 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias2.value);
			series.push(this.combineLine("BIAS2", 'line', bias2, this.biasColor.bias2, index));

			const bias3 = this.chartService.getBIASData(this.quoteBasic, this.biasStat.bias3.value);
			series.push(this.combineLine("BIAS3", 'line', bias3, this.biasColor.bias3, index));
		}

		if (this.maStat) {
			series.push(this.combineLine("MA", "line", this.chartService.getMAData(this.quoteBasic, this.maStat.ma.value), this.maColor.ma));
		}

		if (this.emaStat) {
			series.push(this.combineLine("EMA", "line", this.chartService.getEMAData(this.quoteBasic, this.emaStat.ema.value), this.emaColor.ema));
		}

		this.echarts.clear();

		this.option = Object.assign({}, this.option, {
			xAxis,
			yAxis,
			series
		});
	}

	addLines(name) {
		const tempOption = this.option;
		let series = tempOption['series'];
		//let dataZoom = _.cloneDeep(this.echarts.getOption()['dataZoom']);

		if (name === "BOLL") {
			const bollData = this.chartService.getBollData(this.quoteBasic, this.bollStat.stand.value, this.bollStat.width.value);
			series.push(this.combineLine("MID", "line", bollData[0], this.bollColor.mid));
			series.push(this.combineLine("UP", "line", bollData[1], this.bollColor.up));
			series.push(this.combineLine("LOW", "line", bollData[2], this.bollColor.low));
		}

		if (name === "Line") {
			series.push(this.combineLine("Line", "line", this.chartService.getLineData(this.quoteBasic), this.lineColor.line));
		}

		if (name === "Candle") {
			series.push(this.combineLine("Candle", "candlestick", this.chartService.getCandleData(this.quoteBasic), [this.candleColor.color, this.candleColor.color0]));
		}

		if (name === "MA") {
			series.push(this.combineLine("MA", "line", this.chartService.getMAData(this.quoteBasic, this.maStat.ma.value), this.maColor.ma));
		}

		if (name === "EMA") {
			series.push(this.combineLine("EMA", "line", this.chartService.getEMAData(this.quoteBasic, this.emaStat.ema.value), this.emaColor.ema));
		}

		this.echarts.clear();
		this.option = Object.assign({}, this.option, {
			series,
			//dataZoom
		});
	}

	removeLines(name) {
		const tempOption = this.option;
		let series = tempOption['series'];
		let dataZoom = _.cloneDeep(this.echarts.getOption()['dataZoom']);

		if (name === "BOLL") {
			series = series.filter(item => item.name !== "UP" && item.name !== "MID" && item.name !== "LOW");
		}

		if (name === "Candle") {
			series = series.filter(item => item.name !== "Candle");
		}

		if (name === "Line") {
			series = series.filter(item => item.name !== "Line");
		}

		if (name === "MA") {
			series = series.filter(item => item.name !== "MA");
		}

		if (name === "EMA") {
			series = series.filter(item => item.name !== "EMA");
		}

		this.echarts.clear();
		this.option = Object.assign({}, this.option, {
			series,
			dataZoom
		});
	}

	updateLines(name) {
		const tempOption = this.echarts.getOption();
		let series = tempOption['series'];

		if (name === "BOLL") {
			const upIndex = _.findIndex(series, (item) => item['name'] === "UP");
			const lowIndex = _.findIndex(series, (item) => item['name'] === "LOW");
			const midIndex = _.findIndex(series, (item) => item['name'] === "MID");

			const bollData = this.chartService.getBollData(this.quoteBasic, this.bollStat.stand.value, this.bollStat.width.value);

			series[midIndex].data = bollData[0];
			series[upIndex].data = bollData[1];
			series[lowIndex].data = bollData[2];
		}

		if (name === "MACD") {
			const deaIndex = _.findIndex(series, (item) => item['name'] === "DEA");
			const difIndex = _.findIndex(series, (item) => item['name'] === "DIF");
			const macdIndex = _.findIndex(series, (item) => item['name'] === "MACD");

			const data = this.chartService.getMACDData(this.quoteBasic, this.macdStat.long.value, this.macdStat.short.value, this.macdStat.m.value, );

			series[difIndex].data = data[0];
			series[deaIndex].data = data[1];
			series[macdIndex].data = data[2];
		}

		if (name === "RSI") {
			const rsi1Index = _.findIndex(series, (item) => item['name'] === "RSI1");
			const rsi2Index = _.findIndex(series, (item) => item['name'] === "RSI2");
			const rsi3Index = _.findIndex(series, (item) => item['name'] === "RSI3");

			series[rsi1Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi1.value);
			series[rsi2Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi2.value);
			series[rsi3Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi3.value);
		}

		if (name === "BIAS") {
			const bias1Index = _.findIndex(series, (item) => item['name'] === "BIAS1");
			const bias2Index = _.findIndex(series, (item) => item['name'] === "BIAS2");
			const bias3Index = _.findIndex(series, (item) => item['name'] === "BIAS3");

			series[bias1Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi1.value);
			series[bias2Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi2.value);
			series[bias3Index].data = this.chartService.getRSIData(this.quoteBasic, this.rsiStat.rsi3.value);
		}

		if (name === "MA") {
			const index = _.findIndex(series, (item) => item['name'] === "MA");
			series[index].data = this.chartService.getMAData(this.quoteBasic, this.maStat.ma.value);
		}

		if (name === "EMA") {
			const index = _.findIndex(series, (item) => item['name'] === "EMA");
			series[index].data = this.chartService.getEMAData(this.quoteBasic, this.emaStat.ema.value);
		}

		this.echarts.clear();
		this.option = Object.assign({}, this.option, {
			series
		});
	}

	updatePoint() {
		const tempOption = _.cloneDeepWith(this.echarts.getOption());

		let xAxis = tempOption['xAxis'];
		let yAxis = tempOption['yAxis'];
		let series = tempOption['series'];
		let grid = tempOption['grid'];
		let dataZoom = tempOption['dataZoom'];

		xAxis = xAxis.map(item => {
			item.data = this.quoteBasic.getTimeString(this.ranges[this.rangeIndex].show);
			return item;
		});

		if (this.lineTypeIndex == 0) {
			let index = _.findIndex(series, (item) => item['name'] === 'Candle');
			series[index].data = this.chartService.getCandleData(this.quoteBasic);
		} else {
			let index = _.findIndex(series, (item) => item['name'] === 'Line');
			series[index].data = this.chartService.updateLineData(this.quoteBasic, series[index].data);
		}

		if (this.macdStat) {
			let difIndex = _.findIndex(series, (item) => item['name'] === 'DIF');
			let deaIndex = _.findIndex(series, (item) => item['name'] === 'DEA');
			let macdIndex = _.findIndex(series, (item) => item['name'] === 'MACD');

			const data = this.chartService.updateMACDDAta(this.quoteBasic, this.macdStat.long.value, this.macdStat.short.value, this.macdStat.m.value, series[difIndex].data, series[deaIndex].data, series[macdIndex].data);
			series[difIndex].data = data[0];
			series[deaIndex].data = data[1];
			series[macdIndex].data = data[2];
		}

		if (this.bollStat) {
			let midIndex = _.findIndex(series, (item) => item['name'] === 'MID');
			let upIndex = _.findIndex(series, (item) => item['name'] === 'UP');
			let lowIndex = _.findIndex(series, (item) => item['name'] === 'LOW');

			const bollData = this.chartService.updateBollData(this.quoteBasic, this.bollStat.stand.value, this.bollStat.width.value, series[midIndex].data, series[upIndex].data, series[lowIndex].data);
			series[midIndex].data = bollData[0];
			series[upIndex].data = bollData[1];
			series[lowIndex].data = bollData[2];
		}

		if (this.rsiStat) {
			let rsi1Index = _.findIndex(series, (item) => item['name'] === 'RSI1');
			let rsi2Index = _.findIndex(series, (item) => item['name'] === 'RSI2');
			let rsi3Index = _.findIndex(series, (item) => item['name'] === 'RSI3');

			series[rsi1Index].data = this.chartService.updateRSIData(this.quoteBasic, this.rsiStat.rsi1.value, series[rsi1Index].data);
			series[rsi2Index].data = this.chartService.updateRSIData(this.quoteBasic, this.rsiStat.rsi2.value, series[rsi2Index].data);
			series[rsi3Index].data = this.chartService.updateRSIData(this.quoteBasic, this.rsiStat.rsi3.value, series[rsi3Index].data);
		}

		if (this.biasStat) {
			let bias1Index = _.findIndex(series, (item) => item['name'] === 'BIAS1');
			let bias2Index = _.findIndex(series, (item) => item['name'] === 'BIAS2');
			let bias3Index = _.findIndex(series, (item) => item['name'] === 'BIAS3');

			if (bias1Index > -1 && bias2Index > -1 && bias3Index > -1) {
				series[bias1Index].data = this.chartService.updateBIASData(this.quoteBasic, this.biasStat.bias1.value, series[bias1Index].data);
				series[bias2Index].data = this.chartService.updateBIASData(this.quoteBasic, this.biasStat.bias2.value, series[bias2Index].data);
				series[bias3Index].data = this.chartService.updateBIASData(this.quoteBasic, this.biasStat.bias3.value, series[bias3Index].data);
			}
		}

		if (this.maStat) {
			let index = _.findIndex(series, (item) => item['name'] === 'MA');
			series[index].data = this.chartService.updateMAData(this.quoteBasic, this.maStat.ma.value, series[index].data);
		}

		if (this.emaStat) {
			let index = _.findIndex(series, (item) => item['name'] === 'EMA');
			series[index].data = this.chartService.updateEMAData(this.quoteBasic, this.emaStat.ema.value, series[index].data);
		}

		this.option = Object.assign({}, this.option, {
			xAxis,
			series,
			dataZoom
		});
	}

	resetConfig() {
		this.rangeIndex = 0;
		this.lineTypeIndex = 0;

		this.macdStat = null;
		this.bollStat = null;
		this.rsiStat = null;
		this.biasStat = null;
		this.maStat = null;
		this.emaStat = null;

		const interval = this.ranges[0].interval;
		const eTime = moment().valueOf();
		// const sTime = eTime - this.ranges[0].range;
		const sTime = 0;

		this.option = _.cloneDeep(BASE_OPTION);

		this.echarts.clear();
		this.loadData(this.symbol, interval, sTime, eTime);
	}

	combineLine(name, type, data, color, index = null) {
		const result = {
			name: name,
			type: type,
			data: data,
			xAxisIndex: index,
			yAxisIndex: index,
			itemStyle: {
				normal: {
					color: color,
					borderColor: null,
					color0: null,
					borderColor0: null,
				}
			}
		}
		if (index) {
			result.xAxisIndex = index;
			result.yAxisIndex = index;
		}

		if (typeof color === "string") {
			result.itemStyle.normal.color = color;
		} else {
			result.itemStyle.normal.color = color[0];
			result.itemStyle.normal.color0 = color[1];
		}
		return result;
	}

}
