import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad } from '@angular/router';
import { getLocalItem } from 'utils';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private router:Router) {

  }

  canActivate() {
    return this.hasAuth();
  }


  canLoad() {
    return this.hasAuth();
  }

  hasAuth() {
    if (!getLocalItem('auth')) {
      this.router.navigate(['/index/login']);
      return false;
    }
    return true;
  }

}
