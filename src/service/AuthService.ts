import { Injectable } from '@angular/core';
import { getLocalItem, setLocalItem } from 'utils';

@Injectable()
export class AuthService {

  private userInfo:any = null;

  constructor() {
    if(getLocalItem('userInfo')){
      this.userInfo = getLocalItem('userInfo');
    }
  }

  setUserInfo(userInfo) {
    this.userInfo = userInfo;
    setLocalItem('userInfo', userInfo);
  }

  getUserInfo() {
    return this.userInfo;
  }
}
