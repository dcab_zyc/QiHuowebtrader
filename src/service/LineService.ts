import { Injectable } from '@angular/core';
import { QuoteBasic } from 'service/QuoteBasic';

import { ILineService } from 'interface/ILineService';

@Injectable()
export class LineService implements ILineService {

  connection;

  constructor() {

  }

  compute(quote:QuoteBasic):Array<any> {
    const times = quote.time;
    const close = quote.close;
    const list = [];
    const count = times.length;

    for (let i=0; i<count; i++) {
      list.push(close[i]);
    }

    return list;
  }

  update(quote:QuoteBasic, oldList:Array<any>):Array<any> {
    const orignCount = quote.close.length;
    const close = quote.close;
    const oldCount = oldList.length;
    const times = quote.time;

    if (orignCount === oldCount) {
      oldList[oldCount-1] = close[oldCount-1];
    } else {
      let addCount = orignCount -oldCount
      for (let i = 0; i < addCount; i++) {
        oldList.push(close[oldCount+i]);
      }
    }

    return oldList;
  }
}
