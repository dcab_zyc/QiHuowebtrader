import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService } from 'service/HttpService';
import { CandleService } from 'service/CandleService';
import { MAService } from 'service/MAService';
import { BollService } from 'service/BollService';
import { RSIService } from 'service/RSIService';
import { BIASService } from 'service/BIASService';
import { EMAService } from 'service/EMAService';
import { MACDService } from 'service/MACDService';
import { QuoteBasic } from 'service/QuoteBasic';
// import { ApiRequest } from 'service/ApiRequest';
import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';
import { LineService } from 'service/LineService';

import { IStatService } from 'interface/IStatService';

@Injectable()
export class StatService implements IStatService {
  apiToUrl: any;
  connection;

  constructor(private httpService: HttpService,
    private candleService: CandleService,
    private maService: MAService,
    private bollService: BollService,
    private rsiService: RSIService,
    private biasService: BIASService,
    private emaService: EMAService,
    private macdService: MACDService,
    private apiRequest: ApiRequest,
    private lineService: LineService) {

  }

  //设置基本参数
  setParams(HTTP_URL: string, BASE_API: string) {
    this.apiRequest.setInitializationOption(HTTP_URL, BASE_API);
  }

  //初始化
  async init(): Promise<boolean> {
    let flag: boolean;
    await this.apiRequest.init().then(res => {
      flag = res;
      this.apiToUrl = this.apiRequest.apiToUrl;
    }).catch(err => {
      throw new Error(err);
    });
    return flag;
  }

  async getHistoricalQuote(symbol: string, interval: number, sTime: number, eTime: number, maxCount: number = 1000): Promise<QuoteBasic> {
    let quoteBasic: QuoteBasic;
    await this.apiRequest.QHQuoteApiRequest.getHistoricalQuote(interval, symbol, sTime, eTime, maxCount).then(res => {
      quoteBasic = new QuoteBasic(res.Symbol, res.Interval, res.Time, res.Open, res.Low, res.High, res.Close);
    }).catch(err => {
      throw new Error(err);
    })
    return quoteBasic;
  }

  getCandleData(quote: QuoteBasic): Array<number> {
    return this.candleService.compute(quote);
  }

  getMAData(quote: QuoteBasic, day: number): Array<number> {
    return this.maService.compute(quote, day);
  }

  updateMAData(quote: QuoteBasic, day: number, oldList: Array<number>): Array<number> {
    return this.maService.update(quote, day, oldList);
  }

  getBollData(quote: QuoteBasic, day: number, k: number): Array<any> {
    return this.bollService.compute(quote, day, k);
  }

  updateBollData(quote: QuoteBasic, day: number, k: number, mdList: Array<number>, upList: Array<number>, downList: Array<number>): Array<any> {
    return this.bollService.update(quote, day, k, mdList, upList, downList);
  }

  getRSIData(quote: QuoteBasic, day: number): Array<number> {
    return this.rsiService.compute(quote, day);
  }

  updateRSIData(quote: QuoteBasic, day: number, oldList: Array<number>): Array<number> {
    return this.rsiService.update(quote, day, oldList);
  }

  getBIASData(quote: QuoteBasic, day: number): Array<number> {
    return this.biasService.compute(quote, day);
  }

  updateBIASData(quote: QuoteBasic, day: number, oldList: Array<number>): Array<number> {
    return this.biasService.update(quote, day, oldList);
  }

  getEMAData(quote: QuoteBasic, day: number): Array<number> {
    return this.emaService.compute(quote, day);
  }

  updateEMAData(quote: QuoteBasic, day: number, oldList: Array<number>): Array<number> {
    return this.emaService.update(quote, day, oldList);
  }

  getMACDData(quote: QuoteBasic, lday: number, sday: number, mday: number): Array<any> {
    return this.macdService.compute(quote, lday, sday, mday);
  }

  updateMACDDAta(quote: QuoteBasic, lday: number, sday: number, mday: number, difList: Array<number>, deaList: Array<number>, macdList: Array<number>): Array<any> {
    return this.macdService.update(quote, lday, sday, mday, difList, deaList, macdList);
  }

  getLineData(quote: QuoteBasic): Array<number> {
    return this.lineService.compute(quote);
  }

  updateLineData(quote: QuoteBasic, oldList: Array<any>) {
    return this.lineService.update(quote, oldList);
  }
}