// import the packages  
import {
    Injectable,
    EventEmitter
} from '@angular/core';

@Injectable()
export class SignalRService {

    private proxy: any;
    private proxyName: string;          // = 'forexHub';  
    private connection: any;

    constructor(proxyName: string) {
        this.connection = $.hubConnection("http://114.55.146.142:2002/signalr", {
            useDefaultPath: false
        });

        this.proxyName = proxyName;
        this.proxy = this.connection.createHubProxy(proxyName);
    }

    // method to hit from client  
    public subscribe(symbol) {
        // server side hub method using proxy.invoke with method name pass as param  
        this.proxy.invoke('subscribe', symbol, true);
    };

    public unSubscribe(symbol) {
        this.proxy.invoke('unsubscribe', symbol);
    }
    // check in the browser console for either signalr connected or not  
    public getConnection() {
        return this.connection.start();
    }

    public registerOn(event, func): void {
        this.proxy.on(event, func);
    }
}  