import { IQuoteBasic } from 'interface/IQuoteBasic';

export class QuoteBasic implements IQuoteBasic {
  symbol:string;
  interval:number = 60;

  time:Array<number> = [];
  open:Array<number> = [];
  low:Array<number> = [];
  high:Array<number> = [];
  close:Array<number> = [];

  constructor(symbol, interval, time, open, low, high, close) {
    this.symbol = symbol;
    this.interval = interval * 1000;
    this.time = time;
    this.open = open;
    this.low = low;
    this.high = high;
    this.close = close;

    const count = this.getCount();
    if(count>2 && this.getLastTime() - this.time[count-2] < this.interval){
      this.time.splice(count-1,1);
      this.open.splice(count-1,1);
      this.close[count-2] = this.close[count-1];
      this.close.splice(count-1,1);
      this.high[count - 2] = (this.high[count - 2] < this.high[count - 1]) ? this.high[count - 1] : this.high[count - 2];
      this.high.splice(count-1,1);
      this.low[count - 2] = (this.low[count - 2] > this.low[count - 1]) ? this.low[count - 1] : this.low[count - 2];
      this.low.splice(count-1,1);
    }
  }

  append(symbol, time, price):boolean {
    const endTime = this.getLastTime();
    const startTime = endTime - this.interval;
    if (this.symbol !== symbol || time <= endTime) {
      return false;
    }

    const count = this.getCount();

    if (count === 0) {
      this.time.push(Math.floor(time/this.interval)*this.interval);
      this.open.push(price);
      this.close.push(price);
      this.low.push(price);
      this.high.push(price);
      return true;
    }

    if(time - endTime < this.interval) {
      this.close[count - 1] = price;
      this.high[count - 1] = (this.high[count - 1] < price) ? price : this.high[count - 1];
      this.low[count - 1] = (this.low[count - 1] > price) ? price : this.low[count - 1];
      return true;
    }

    const timeSpan = time - endTime;
    const pointCount = Math.floor(timeSpan / this.interval);

    for (let i = 0; i< pointCount; i++) {
      this.time.push(this.time[count - 1] + (this.interval)*(i+1));
      if (i < pointCount -1) {
        this.open.push(this.close[count - 1]);
        this.close.push(this.close[count - 1]);
        this.low.push(this.close[count - 1]);
        this.high.push(this.close[count - 1]);
      } else {
        this.open.push(price);
        this.close.push(price);
        this.low.push(price);
        this.high.push(price);
      }
    }

    return true;
  }

  // append(symbol, time, price):boolean {
  //   const endTime = this.getLastTime();

  //   if (this.symbol !== symbol && time < endTime) {
  //     return false;
  //   }

  //   const count = this.getCount();

  //   if (count === 0) {
  //     this.time.push(Math.floor(time/this.interval)*this.interval);
  //     this.open.push(price);
  //     this.close.push(price);
  //     this.low.push(price);
  //     this.high.push(price);
  //     return true;
  //   }

  //   if (time === endTime) {
  //     this.close[count - 1] = price;
  //     this.high[count - 1] = (this.high[count - 1] < price) ? price : this.high[count - 1];
  //     this.low[count - 1] = (this.low[count - 1] > price) ? price : this.low[count - 1];
  //     return true;
  //   }

  //   const timeRange = time - endTime;
  //   const mod = endTime % this.interval;
  //   const pointCount = Math.ceil((timeRange+mod)/ this.interval);

  //   console.log(pointCount);

  //   if (pointCount === 1) {
  //     this.time[count-1] = time;
  //     this.close[count - 1] = price;
  //     this.high[count - 1] = (this.high[count - 1] < price) ? price : this.high[count - 1];
  //     this.low[count - 1] = (this.low[count - 1] > price) ? price : this.low[count - 1];
  //     return true;
  //   }

  //   for (let i=0; i< pointCount; i++) {
  //     if (i === 0) {
  //       this.time[count-1] = endTime+mod;
  //       continue;
  //     }
  //     if (i < pointCount-1) {
  //       this.time.push(this.time[count-1] + i*this.interval);
  //       this.open.push(this.close[count-1]);
  //       this.close.push(this.close[count-1]);
  //       this.low.push(this.close[count-1]);
  //       this.high.push(this.close[count-1]);
  //       continue;
  //     }

  //     this.time.push(time);
  //     this.open.push(price);
  //     this.close.push(price);
  //     this.low.push(price);
  //     this.high.push(price);
  //   }

  //   return true;
  // }

  getLastTime() {
    if (this.time.length > 0) {
      return this.time[this.time.length - 1];
    }
    return 0;
  }

  getCount() {
    return this.time.length;
  }

  getTimeString(rangeName) {
    let timeString = [];
    for (let i = 0; i < this.time.length; i++) {
        let time = new Date(this.time[i]).toLocaleString().replace(/:\d{1,2}$/,'');
        if(rangeName.indexOf('分') !== -1){
          time = time.replace(' ','\n').substring(5);
        } else {
          time = time.substring(0,time.length - 5);
        }
        timeString[i] = time;
    }
    return timeString;
  }
};
