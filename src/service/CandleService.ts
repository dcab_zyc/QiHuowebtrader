import { Injectable } from '@angular/core';

import { QuoteBasic } from 'service/QuoteBasic';
import { ICandleService } from 'interface/ICandleService';

@Injectable()
export class CandleService implements ICandleService {

  connection;

  constructor() {

  }

  compute(quote:QuoteBasic):Array<any> {
    const candleList = [];
    const timeList = quote.time;
    const open = quote.open;
    const close = quote.close;
    const high = quote.high;
    const low = quote.low;
    const count = timeList.length;

    for (let i=0; i<count; i++) {
      const dayData = [];
      // dayData.push(timeList[i]);
      dayData.push(open[i]);
      dayData.push(close[i]);
      dayData.push(low[i]);
      dayData.push(high[i]);
      candleList.push(dayData);
    }
    return candleList;
  }
}
