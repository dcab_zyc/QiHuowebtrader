import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';

import { } from 'jasmine';

// Load the implementations that should be tested
import { QuoteBasic } from 'service/QuoteBasic';
import { StatService } from 'service/StatService';
import { MAService } from 'service/MAService';
import { BollService } from 'service/BollService';
import { RSIService } from 'service/RSIService';
import { EMAService } from 'service/EMAService';
import { MACDService } from 'service/MACDService';
import { BIASService } from 'service/BIASService';
import { TEST_CHARTDATA,
  TEST_CHARTDATA_EMA_SUCCESS,
  TEST_CHARTDATA_MA_SUCCESS,
  TEST_CHARTDATA_ZERO_ONE,
  TEST_CHARTDATA_ZERO_THREE,
  TEST_CHARTDATA_RSI_ZERO_ONE,
  TEST_CHARTDATA_MACD_SUCCESS,
  TEST_CHARTDATA_BIAS_SUCCESS,
  TEST_CHARTDATA_BOLL_SUCCESS,
  TEST_CHARTDATA_RSI_SUCCESS,
  TEST_CHARTDATA_ZERO,
  TEST_CHARTDATA_UNDERZERO,
  TEST_CHARTDATA_UNDERZERO_ONE,
  TEST_CHARTDATA_UNDERZERO_THREE,
  TEST_CHARTDATA_ONE,
  TEST_CHARTDATA_ONE_ONE,
  TEST_CHARTDATA_ONE_THREE,
  TEST_CHARTDATA_RSI_ONE_ONE,
  TEST_CHARTDATA_BIAS_ONE_ONE,
  TEST_CHARTDATA_MACD_ONE_THREE,
  TEST_CHARTDATA_NEW,
  TEST_CHARTDATA_MA_UPDATE_SUCCESS,
  TEST_CHARTDATA_BOLL_UPDATE_SUCCESS,
  TEST_CHARTDATA_RSI_UPDATE_SUCCESS,
  TEST_CHARTDATA_BIAS_UPDATE_SUCCESS,
  TEST_CHARTDATA_EMA_UPDATE_SUCCESS,
  TEST_CHARTDATA_MACD_UPDATE_SUCCESS
} from 'utils/chart-config';
import {
  MA_STAT,
  BOLL_STAT,
  RSI_STAT,
  BIAS_STAT,
  EMA_STAT,
  MACD_STAT
} from 'utils';

describe(`StatService`, () => {
  // let service: StatService;
  let maService: MAService = new MAService();
  let bollService: BollService = new BollService();
  let rsiService: RSIService = new RSIService();
  let emaService: EMAService = new EMAService();
  let biasService: BIASService = new BIASService(emaService);
  let macdService: MACDService = new MACDService(emaService);

  let quote: QuoteBasic = new QuoteBasic(TEST_CHARTDATA.Symbol, TEST_CHARTDATA.Interval, TEST_CHARTDATA.Time, TEST_CHARTDATA.Open, TEST_CHARTDATA.Low, TEST_CHARTDATA.High, TEST_CHARTDATA.Close);
  let quoteZero: QuoteBasic = new QuoteBasic(TEST_CHARTDATA_ZERO.Symbol, TEST_CHARTDATA_ZERO.Interval, TEST_CHARTDATA_ZERO.Time, TEST_CHARTDATA_ZERO.Open, TEST_CHARTDATA_ZERO.Low, TEST_CHARTDATA_ZERO.High, TEST_CHARTDATA_ZERO.Close);
  let quoteNull: QuoteBasic = new QuoteBasic(TEST_CHARTDATA.Symbol, TEST_CHARTDATA.Interval, [], [], [], [], []);
  let quoteUnderZero: QuoteBasic = new QuoteBasic(TEST_CHARTDATA_UNDERZERO.Symbol, TEST_CHARTDATA_UNDERZERO.Interval, TEST_CHARTDATA_UNDERZERO.Time, TEST_CHARTDATA_UNDERZERO.Open, TEST_CHARTDATA_UNDERZERO.Low, TEST_CHARTDATA_UNDERZERO.High, TEST_CHARTDATA_UNDERZERO.Close);
  let quoteOne: QuoteBasic = new QuoteBasic(TEST_CHARTDATA_ONE.Symbol, TEST_CHARTDATA_ONE.Interval, TEST_CHARTDATA_ONE.Time, TEST_CHARTDATA_ONE.Open, TEST_CHARTDATA_ONE.Low, TEST_CHARTDATA_ONE.High, TEST_CHARTDATA_ONE.Close);

  let quoteNew: QuoteBasic = new QuoteBasic(TEST_CHARTDATA_NEW.Symbol, TEST_CHARTDATA_NEW.Interval, TEST_CHARTDATA_NEW.Time, TEST_CHARTDATA_NEW.Open, TEST_CHARTDATA_NEW.Low, TEST_CHARTDATA_NEW.High, TEST_CHARTDATA_NEW.Close);

  describe(`MA`, () => {
    describe(`getMAData`, () => {
      // 获取ma线成功
      it(`getMADataSuccess`, () => {
        expect(maService.compute(quote, MA_STAT['ma'].value)).toEqual(TEST_CHARTDATA_MA_SUCCESS);
      });
      // 获取ma线为空
      it(`getMADataNull`, () => {
        expect(maService.compute(quoteNull, MA_STAT['ma'].value)).toEqual([]);
      });
      // 获取ma线为0
      it(`getMADataZero`, () => {
        expect(maService.compute(quoteZero, MA_STAT['ma'].value)).toEqual(TEST_CHARTDATA_ZERO_ONE);
      });
      // 获取ma线为负数
      it(`getMADataUnderZero`, () => {
        expect(maService.compute(quoteUnderZero, MA_STAT['ma'].value)).toEqual(TEST_CHARTDATA_UNDERZERO_ONE);
      });
      // 获取ma线参数为0
      it(`getMADataParamZero`, () => {
        expect(maService.compute(quote, 0)).toEqual([]);
      });
      // 获取ma线源数据只有一个
      it(`getMADataOne`, () => {
        expect(maService.compute(quoteOne, MA_STAT['ma'].value)).toEqual(TEST_CHARTDATA_ONE_ONE);
      });
    });
    describe(`updateMAData`, () => {
      // 更新ma线成功
      it(`updateMADataSuccess`, () => {
        expect(maService.update(quoteNew, MA_STAT['ma'].value, TEST_CHARTDATA_MA_SUCCESS)).toEqual(TEST_CHARTDATA_MA_UPDATE_SUCCESS);
      });
      // 更新ma线oldList为空
      it(`updateMADataOldListNull`, () => {
        expect(maService.update(quoteNew, MA_STAT['ma'].value, [])).toEqual(TEST_CHARTDATA_MA_UPDATE_SUCCESS);
      });
      // 更新ma线day为0
      it(`updateMADataDayNull`, () => {
        expect(maService.update(quoteNew, 0, TEST_CHARTDATA_MA_SUCCESS)).toEqual(TEST_CHARTDATA_MA_SUCCESS);
      });
      // 更新ma线quote为空
      it(`updateMADataQuoteNull`, () => {
        expect(maService.update(quoteNull, MA_STAT['ma'].value, TEST_CHARTDATA_MA_SUCCESS)).toEqual(TEST_CHARTDATA_MA_SUCCESS);
      });
      // 更新ma线源数据只有一个
      it(`updateMADataOne`, () => {
        expect(maService.update(quoteOne, MA_STAT['ma'].value, TEST_CHARTDATA_MA_SUCCESS)).toEqual(TEST_CHARTDATA_MA_SUCCESS);
      });
    });
  });

  describe(`BOLL`, () => {
    describe(`getBollData`, () => {
      // 获取boll线成功
      it(`getBollDataSuccess`, () => {
        expect(bollService.compute(quote, 3, BOLL_STAT['width'].value)).toEqual(TEST_CHARTDATA_BOLL_SUCCESS);
      });
      // 获取boll线为空
      it(`getBollDataNull`, () => {
        expect(bollService.compute(quoteNull, 3, BOLL_STAT['width'].value)).toEqual([[],[],[]]);
      });
      // 获取boll线为0
      it(`getBollDataZero`, () => {
        expect(bollService.compute(quoteZero, 3, BOLL_STAT['width'].value)).toEqual(TEST_CHARTDATA_ZERO_THREE);
      });
      // 获取boll线为负数
      it(`getBollDataUnderZero`, () => {
        expect(bollService.compute(quoteUnderZero, 3, BOLL_STAT['width'].value)).toEqual(TEST_CHARTDATA_UNDERZERO_THREE);
      });
      // 获取boll线参数为0
      it(`getBollDataParamZero`, () => {
        expect(bollService.compute(quote, 0, 0)).toEqual([[],[],[]]);
      });
      // 获取boll线源数据只有一个
      it(`getBollDataOne`, () => {
        expect(bollService.compute(quoteOne, 3, BOLL_STAT['width'].value)).toEqual(TEST_CHARTDATA_ONE_THREE);
      });
    });
    describe(`updateBollData`, () => {
      // 更新boll线成功
      it(`updateBollDataSuccess`, () => {
        expect(bollService.update(quoteNew, 3, BOLL_STAT['width'].value, TEST_CHARTDATA_BOLL_SUCCESS[0], TEST_CHARTDATA_BOLL_SUCCESS[1], TEST_CHARTDATA_BOLL_SUCCESS[2])).toEqual(TEST_CHARTDATA_BOLL_UPDATE_SUCCESS);
      });
      // 更新boll线oldList为空
      it(`updateBollDataOldListNull`, () => {
        expect(bollService.update(quoteNew, 3, BOLL_STAT['width'].value, [], [], [])).toEqual(TEST_CHARTDATA_BOLL_UPDATE_SUCCESS);
      });
      // 更新boll线day为0
      it(`updateBollDataDayNull`, () => {
        expect(bollService.update(quoteNew, 0, BOLL_STAT['width'].value, TEST_CHARTDATA_BOLL_SUCCESS[0], TEST_CHARTDATA_BOLL_SUCCESS[1], TEST_CHARTDATA_BOLL_SUCCESS[2])).toEqual(TEST_CHARTDATA_BOLL_SUCCESS);
      });
      // 更新boll线quote为空
      it(`updateBollDataQuoteNull`, () => {
        expect(bollService.update(quoteNull, 3, BOLL_STAT['width'].value, TEST_CHARTDATA_BOLL_SUCCESS[0], TEST_CHARTDATA_BOLL_SUCCESS[1], TEST_CHARTDATA_BOLL_SUCCESS[2])).toEqual(TEST_CHARTDATA_BOLL_SUCCESS);
      });
      // 更新boll线源数据只有一个
      it(`updateBollDataOne`, () => {
        expect(bollService.update(quoteOne, 3, BOLL_STAT['width'].value, TEST_CHARTDATA_BOLL_SUCCESS[0], TEST_CHARTDATA_BOLL_SUCCESS[1], TEST_CHARTDATA_BOLL_SUCCESS[2])).toEqual(TEST_CHARTDATA_BOLL_SUCCESS);
      });
    });
  });

  describe(`RSI`, () => {
    describe(`getRSIData`, () => {
      // 获取rsi线成功
      it(`getRSIDataSuccess`, () => {
        expect(rsiService.compute(quote, 6)).toEqual(TEST_CHARTDATA_RSI_SUCCESS);
      });
      // 获取rsi线为空
      it(`getRSIDataNull`, () => {
        expect(rsiService.compute(quoteNull, 6)).toEqual([]);
      });
      // 获取rsi线为0
      it(`getRSIDataZero`, () => {
        expect(rsiService.compute(quoteZero, 6)).toEqual(TEST_CHARTDATA_RSI_ZERO_ONE);
      });
      // 获取rsi线为负数
      it(`getRSIDataUnderZero`, () => {
        expect(rsiService.compute(quoteUnderZero, 6)).toEqual(TEST_CHARTDATA_RSI_ZERO_ONE);
      });
      // 获取rsi线参数为0
      it(`getRSIDataParamZero`, () => {
        expect(rsiService.compute(quote, 0)).toEqual([]);
      });
      // 获取rsi线源数据只有一个
      it(`getRSIDataOne`, () => {
        expect(rsiService.compute(quoteOne, 6)).toEqual(TEST_CHARTDATA_RSI_ONE_ONE);
      });
    });
    describe(`updateRSIData`, () => {
      // 更新rsi线成功
      it(`updateRSIDataSuccess`, () => {
        expect(rsiService.update(quoteNew, 6, TEST_CHARTDATA_RSI_SUCCESS)).toEqual(TEST_CHARTDATA_RSI_UPDATE_SUCCESS);
      });
      // 更新rsi线oldList为空
      it(`updateRSIDataOldListNull`, () => {
        expect(rsiService.update(quoteNew, 6, [])).toEqual(TEST_CHARTDATA_RSI_UPDATE_SUCCESS);
      });
      // 更新rsi线day为0
      it(`updateRSIDataDayNull`, () => {
        expect(rsiService.update(quoteNew, 0, TEST_CHARTDATA_RSI_SUCCESS)).toEqual(TEST_CHARTDATA_RSI_SUCCESS);
      });
      // 更新rsi线quote为空
      it(`updateRSIDataQuoteNull`, () => {
        expect(rsiService.update(quoteNull, 6, TEST_CHARTDATA_RSI_SUCCESS)).toEqual(TEST_CHARTDATA_RSI_SUCCESS);
      });
      // 更新rsi线源数据只有一个
      it(`updateRSIDataOne`, () => {
        expect(rsiService.update(quoteOne, 6, TEST_CHARTDATA_RSI_SUCCESS)).toEqual(TEST_CHARTDATA_RSI_SUCCESS);
      });
    });
  });

  describe(`BIAS`, () => {
    describe(`getBIASData`, () => {
      // 获取bias线成功
      it(`getBIASDataSuccess`, () => {
        expect(biasService.compute(quote, 3)).toEqual(TEST_CHARTDATA_BIAS_SUCCESS);
      });
      // 获取bias线为空
      it(`getBIASDataNull`, () => {
        expect(biasService.compute(quoteNull, 3)).toEqual([]);
      });
      // 获取bias线为0
      it(`getBIASDataZero`, () => {
        expect(biasService.compute(quoteZero, 3)).toEqual(TEST_CHARTDATA_ZERO_ONE);
      });
      // 获取bias线为负数
      it(`getBIASDataUnderZero`, () => {
        expect(biasService.compute(quoteUnderZero, 3)).toEqual(TEST_CHARTDATA_ZERO_ONE);
      });
      // 获取bias线参数为0
      it(`getBIASDataParamZero`, () => {
        expect(biasService.compute(quote, 0)).toEqual([]);
      });
      // 获取bias线源数据只有一个
      it(`getBIASDataOne`, () => {
        expect(biasService.compute(quoteOne, 3)).toEqual(TEST_CHARTDATA_BIAS_ONE_ONE);
      });
    });
    describe(`updateBIASData`, () => {
      // 更新bias线成功
      it(`updateBIASDataSuccess`, () => {
        expect(biasService.update(quoteNew, 3, TEST_CHARTDATA_BIAS_SUCCESS)).toEqual(TEST_CHARTDATA_BIAS_UPDATE_SUCCESS);
      });
      // 更新bias线oldList为空
      it(`updateBIASDataOldListNull`, () => {
        expect(biasService.update(quoteNew, 3, [])).toEqual(TEST_CHARTDATA_BIAS_UPDATE_SUCCESS);
      });
      // 更新bias线day为0
      it(`updateBIASDataDayNull`, () => {
        expect(biasService.update(quoteNew, 0, TEST_CHARTDATA_BIAS_SUCCESS)).toEqual(TEST_CHARTDATA_BIAS_SUCCESS);
      });
      // 更新bias线quote为空
      it(`updateBIASDataQuoteNull`, () => {
        expect(biasService.update(quoteNull, 3, TEST_CHARTDATA_BIAS_SUCCESS)).toEqual(TEST_CHARTDATA_BIAS_SUCCESS);
      });
      // 更新bias线源数据只有一个
      it(`updateBIASDataOne`, () => {
        expect(biasService.update(quoteOne, 3, TEST_CHARTDATA_BIAS_SUCCESS)).toEqual(TEST_CHARTDATA_BIAS_SUCCESS);
      });
    });
  });

  describe(`EMA`, () => {
    describe(`getEMAData`, () => {
      // 获取ema线成功
      it(`getEMADataSuccess`, () => {
        expect(emaService.compute(quote, EMA_STAT['ema'].value)).toEqual(TEST_CHARTDATA_EMA_SUCCESS);
      });
      // 获取ema线为空
      it(`getEMADataNull`, () => {
        expect(emaService.compute(quoteNull, EMA_STAT['ema'].value)).toEqual([]);
      });
      // 获取ema线为0
      it(`getEMADataZero`, () => {
        expect(emaService.compute(quoteZero, EMA_STAT['ema'].value)).toEqual(TEST_CHARTDATA_ZERO_ONE);
      });
      // 获取ema线为负数
      it(`getEMADataZero`, () => {
        expect(emaService.compute(quoteUnderZero, EMA_STAT['ema'].value)).toEqual(TEST_CHARTDATA_UNDERZERO_ONE);
      });
      // 获取ema线参数为0
      it(`getEMADataParamZero`, () => {
        expect(emaService.compute(quote, 0)).toEqual([]);
      });
      // 获取ema线源数据只有一个
      it(`getEMADataOne`, () => {
        expect(emaService.compute(quoteOne, 3)).toEqual(TEST_CHARTDATA_ONE_ONE);
      });
    });
    describe(`updateEMAData`, () => {
      // 更新ema线成功
      it(`updateEMADataSuccess`, () => {
        expect(emaService.update(quoteNew, EMA_STAT['ema'].value, TEST_CHARTDATA_EMA_SUCCESS)).toEqual(TEST_CHARTDATA_EMA_UPDATE_SUCCESS);
      });
      // 更新ema线oldList为空
      it(`updateEMADataOldListNull`, () => {
        expect(emaService.update(quoteNew, EMA_STAT['ema'].value, [])).toEqual(TEST_CHARTDATA_EMA_UPDATE_SUCCESS);
      });
      // 更新ema线day为0
      it(`updateEMADataDayNull`, () => {
        expect(emaService.update(quoteNew, 0, TEST_CHARTDATA_EMA_SUCCESS)).toEqual(TEST_CHARTDATA_EMA_SUCCESS);
      });
      // 更新ema线quote为空
      it(`updateEMADataQuoteNull`, () => {
        expect(emaService.update(quoteNull, EMA_STAT['ema'].value, TEST_CHARTDATA_EMA_SUCCESS)).toEqual(TEST_CHARTDATA_EMA_SUCCESS);
      });
      // 更新ema线源数据只有一个
      it(`updateEMADataOne`, () => {
        expect(emaService.update(quoteOne, EMA_STAT['ema'].value, TEST_CHARTDATA_EMA_SUCCESS)).toEqual(TEST_CHARTDATA_EMA_SUCCESS);
      });
    });
  });

  describe(`MACD`, () => {
    describe(`getMACDData`, () => {
      // 获取macd线成功
      it(`getMACDDataSuccess`, () => {
        expect(macdService.compute(quote, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value)).toEqual(TEST_CHARTDATA_MACD_SUCCESS);
      });
      // 获取macd线为空
      it(`getMACDDataNull`, () => {
        expect(macdService.compute(quoteNull, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value)).toEqual([[],[],[]]);
      });
      // 获取macd线为0
      it(`getMACDDataZero`, () => {
        expect(macdService.compute(quoteZero, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value)).toEqual(TEST_CHARTDATA_ZERO_THREE);
      });
      // 获取macd线为负数
      it(`getMACDDataUnderZero`, () => {
        expect(macdService.compute(quoteUnderZero, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value)).toEqual(TEST_CHARTDATA_ZERO_THREE);
      });
      // 获取macd线参数为0
      it(`getMACDDataParamZero`, () => {
        expect(macdService.compute(quote, 0, 0, 0)).toEqual([[],[],[]]);
      });
      // 获取macd线源数据只有一个
      it(`getMACDDataOne`, () => {
        expect(macdService.compute(quoteOne, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value)).toEqual(TEST_CHARTDATA_MACD_ONE_THREE);
      });
    });
    describe(`updateMACDData`, () => {
      // 更新macd线成功
      it(`updateMACDDataSuccess`, () => {
        expect(macdService.update(quoteNew, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value,TEST_CHARTDATA_MACD_SUCCESS[0],TEST_CHARTDATA_MACD_SUCCESS[1],TEST_CHARTDATA_MACD_SUCCESS[2])).toEqual(TEST_CHARTDATA_MACD_UPDATE_SUCCESS);
      });
      // 更新macd线oldList为空
      it(`updateMACDDataOldListNull`, () => {
        expect(macdService.update(quoteNew, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value, [], [], [])).toEqual(TEST_CHARTDATA_MACD_UPDATE_SUCCESS);
      });
      // 更新macd线day为0
      it(`updateMACDDataDayNull`, () => {
        expect(macdService.update(quoteNew, 0, 0, 0, TEST_CHARTDATA_MACD_SUCCESS[0],TEST_CHARTDATA_MACD_SUCCESS[1],TEST_CHARTDATA_MACD_SUCCESS[2])).toEqual(TEST_CHARTDATA_MACD_SUCCESS);
      });
      // 更新macd线quote为空
      it(`updateMACDDataQuoteNull`, () => {
        expect(macdService.update(quoteNull, MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value ,TEST_CHARTDATA_MACD_SUCCESS[0],TEST_CHARTDATA_MACD_SUCCESS[1],TEST_CHARTDATA_MACD_SUCCESS[2])).toEqual(TEST_CHARTDATA_MACD_SUCCESS);
      });
      // 更新macd线源数据只有一个
      it(`updateMACDDataOne`, () => {
        expect(macdService.update(quoteOne,MACD_STAT['long'].value, MACD_STAT['short'].value, MACD_STAT['m'].value, TEST_CHARTDATA_MACD_SUCCESS[0],TEST_CHARTDATA_MACD_SUCCESS[1],TEST_CHARTDATA_MACD_SUCCESS[2])).toEqual(TEST_CHARTDATA_MACD_SUCCESS);
      });
    });
  });

});
