import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, CanActivateChild } from '@angular/Router';

import { ApiRequest } from 'service/ApiRequest';

@Injectable()
export class ApiCanLoad implements CanLoad, CanActivate, CanActivateChild  {
  constructor(private apiRequest:ApiRequest) {

  }

  canLoad () {
    return true;
  }

  canActivate () {
    return true;
  }

  canActivateChild() {
    if (this.apiRequest.apiToUrl === null) {
      return this.apiRequest.init();
    }
    return true;
  }
}