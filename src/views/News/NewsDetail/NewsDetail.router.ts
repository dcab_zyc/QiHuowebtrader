import { Routes } from '@angular/router';
import { NewsDetailComponent } from './NewsDetail.component';

export const ROUTES: Routes = [
  { path: '', component: NewsDetailComponent }
];