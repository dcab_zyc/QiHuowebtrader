import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { ROUTES } from './NewsDetail.router'; 
import { NewsDetailComponent } from './NewsDetail.component';


@NgModule({
  declarations: [
    NewsDetailComponent
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    CommonModule,
    NgxPaginationModule
  ],
  exports: [
  ],
  providers: [
    
  ],
})
export class NewsDetailModule {
  constructor() {
    
  }
}