import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { ROUTES } from './News.router'; 
import { NewsComponent } from './News.component';


@NgModule({
  declarations: [
    NewsComponent
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    CommonModule,
    NgxPaginationModule
  ],
  exports: [
  ],
  providers: [
    
  ],
})
export class NewsModule {
  constructor() {
    
  }
}