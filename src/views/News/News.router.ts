import { Routes } from '@angular/router';
import { NewsComponent } from './News.component';

export const ROUTES: Routes = [
  { path: '', component: NewsComponent },
  { path: ':id', loadChildren: 'views/News/NewsDetail/NewsDetail.module#NewsDetailModule'}
];