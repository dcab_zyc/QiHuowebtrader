import { Component } from '@angular/core';
@Component({
  selector: 'webtrader',
  template: `
  <div class="app">
     <header-menu></header-menu>
      <div class="content">
        <router-outlet></router-outlet>
      </div>
     <footer-bottom></footer-bottom>
   </div>`,
  styleUrls: []
})

export class WebTraderComponent {
  constructor(){
  }
}