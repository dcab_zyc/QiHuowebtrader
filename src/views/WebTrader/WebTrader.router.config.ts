import { Routes } from '@angular/router';

import { ApiCanLoad } from 'service/ApiCanLoad';
import { AuthGuard } from "service/AuthGuard";

import { WebTraderComponent } from './WebTrader.component';

export const ROUTES: Routes = [
  {
    path: '', component: WebTraderComponent, children: [
      { path: '', loadChildren: "views/Main/Main.module#MainModule" },
      { path: 'login', loadChildren: "views/Login/Login.module#LoginModule" },
      { path: 'news', loadChildren: "views/News/News.module#NewsModule" },
      { path: 'user', loadChildren: "views/User/User.module#UserModule", canActivate: [AuthGuard] },
      { path: 'calendar', loadChildren: "views/Calendar/Calendar.module#CalendarModule" },
      { path: 'market', loadChildren: "views/Market/Market.module#MarketModule" },
      { path: 'account', loadChildren: "views/Account/Account.module#AccountModule" }
    ]
  },
];
