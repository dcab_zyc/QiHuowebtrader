import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { WebTraderComponent } from './WebTrader.component';
import { ROUTES } from './WebTrader.router.config';

import { HeaderComponent } from 'components/Header/Header.component';
import { FooterComponent } from 'components/Footer/Footer.component';

@NgModule({
  declarations: [
    WebTraderComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
    HttpModule,
  ],
  exports: [
  ],
  providers: [
  ],
  bootstrap: [WebTraderComponent]
})
export class WebTraderModule {
  constructor() {
  }
}