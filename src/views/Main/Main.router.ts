import { Routes } from '@angular/router';
import { MainComponent } from './Main.component';

export const ROUTES: Routes = [
  { path: '', component: MainComponent }
];