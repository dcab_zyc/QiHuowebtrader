import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ROUTES } from './Main.router'; 
import { MainComponent } from './Main.component';


@NgModule({
  declarations: [
    MainComponent
  ],
  imports:[
    CommonModule,
    RouterModule.forChild(ROUTES),
  ],
  exports: [
  ],
  providers: [
    
  ],
})
export class MainModule {
  constructor() {
    
  }
}