import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';

import { ApiRequest } from 'service/ApiRequest';
import { NewsBases } from 'model/NewsBases';

@Component({
    selector: 'main',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './Main.html',
    styleUrls: []
  }
)

export class MainComponent {

  
  // FOREX 外汇
  forexList: Array<NewsBases> = [];

  // STOCK 股市
  stockList: Array<NewsBases> = [];

  // GOLD 黄金
  goldList: Array<NewsBases> = [];

  eventList = [];

  constructor(private apiRequest:ApiRequest, private title: Title) {

  }

  ngOnInit() {

    this.title.setTitle("主页");

    this.apiRequest.getNewsBases("FOREX").then((data) => {
      this.forexList = <Array<NewsBases>>data;
    }).catch((e) => {
      console.log(e);
    });

    this.apiRequest.getNewsBases("STOCK").then((data) => {
      this.stockList = <Array<NewsBases>>data;
    }).catch((e) => {
      console.log(e);
    });

    this.apiRequest.getNewsBases("GOLD").then((data) => {
      this.goldList = <Array<NewsBases>>data;
    }).catch((e) => {
      console.log(e);
    });

    this.loadEventData();
  }

  loadEventData() {

    const eTime = moment(moment().format('YYYY-MM-DD 23:59:59')).unix();
    const sTime = moment(moment().format('YYYY-MM-DD 00:00:00')).unix();

    this.apiRequest.getCalendarReserveOrder("cd", sTime, eTime, 12).then((data) => {
      this.eventList = data.map((item) => {
        item.Time = this.timeConvert(item.Time);
        return item;
      });
    });
  }

  timeConvert(time) {
    return moment(time * 1000).format("YYYY-MM-DD HH:mm");
  }
}