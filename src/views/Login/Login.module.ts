import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ROUTES } from './Login.router';
import { LoginComponent } from './Login/Login.component';
import { RegisteComponent } from './Registe/Registe.component';
import { HttpService } from 'service/HttpService';

@NgModule({
  declarations: [
    LoginComponent,
    RegisteComponent,
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
  ],
  providers: [
  ],
})
export class LoginModule {
  constructor() {

  }
}
