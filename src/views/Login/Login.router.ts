import { Routes } from '@angular/router';
import { LoginComponent } from './Login/Login.component';
import { RegisteComponent } from './Registe/Registe.component';

export const ROUTES:Routes = [
  {path: '', component: LoginComponent, pathMatch: 'full'},
  {path: 'findpwd', loadChildren: './FindPwd/FindBack.module#FindBackModule', },
  {path: 'registe', component: RegisteComponent, }
];