import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ROUTES } from './FindBack.router';
import { ValidateTelComponent } from './ValidateTel/ValidateTel.component';
import { ResetPwdComponent } from './ResetPwd/ResetPwd.component';
import { LoginService } from './../Login.service';

@NgModule({
  declarations: [
    ValidateTelComponent,
    ResetPwdComponent,
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
  ],
  providers: [
    LoginService
  ],
})
export class FindBackModule {
  constructor() {

  }
}
