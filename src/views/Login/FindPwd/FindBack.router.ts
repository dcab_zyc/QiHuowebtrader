import { Routes } from '@angular/router';
import { ValidateTelComponent } from './ValidateTel/ValidateTel.component';
import { ResetPwdComponent } from './ResetPwd/ResetPwd.component';

export const ROUTES:Routes = [
  {path: '', component: ValidateTelComponent, pathMatch: 'full'},
  {path: 'valtel', component: ValidateTelComponent, },
  {path: 'resetpwd', component: ResetPwdComponent, }
];