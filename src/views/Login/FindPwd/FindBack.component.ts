import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../Login.service';

@Component({
    selector: 'findback',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './FindBack.html',
    styleUrls: []
  }
)

export class FindBackComponent {

  public loginForm:FormGroup;

  constructor(private formBuilder:FormBuilder, private router: Router, private loginService:LoginService) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      name:[null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
    });
  }

  onSubmit(form) {
    console.log(form);
    if(form.valid) {
      //this.loginService.loginIn(form.value);
      this.router.navigate(['back']);
    }
    //return false;
  }

}