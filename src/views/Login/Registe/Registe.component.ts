import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { phoneNumberValidator, passwordMatchValidator } from 'utils/index';
import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'login',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './Registe.html',
    styleUrls: []
  }
)

export class RegisteComponent {

  //loading
  private loading: boolean = false;
  //手机号重复
  private phoneNumberUsed: boolean = false;
  //验证码倒计时
  private countTime: number = 0;
  //后端错误信息
  private error:string = '';

  public registerForm:FormGroup;

  constructor(private formBuilder:FormBuilder, private router: Router, private apiRequest: ApiRequest) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      PhoneNumber:[null, Validators.compose([Validators.required, phoneNumberValidator])],
      VerificationCode: [null, Validators.compose([Validators.required])],
      Password: [null, Validators.compose([Validators.required,Validators.pattern("^.{6,11}$")])],
      PasswordConfirm: [null, Validators.compose([Validators.required])],
    }, { validator: passwordMatchValidator } );
  }

  isPhoneNumberUsed(){
    if(!this.registerForm.controls.PhoneNumber.errors){
      this.apiRequest.isPhoneNumberUsed(this.registerForm.value.PhoneNumber).then((data) => {
        this.phoneNumberUsed = data;
      });
    }
  }

  requestVerificationCodeForRegister() {
    let self = this;
    this.apiRequest.requestVerificationCodeForRegister(this.registerForm.value.PhoneNumber).then((data) => {
      self.countTime = 60;
      let count = setInterval(function(){
        if(self.countTime > 0){
          self.countTime --;
        } else if (self.countTime === 0) {
          clearInterval(count);
        }
      },1000);
    });
  }

  // 清除输入框内容
  clear(key) {
    const value = this.registerForm.value;
    value[key] = '';
    this.registerForm.setValue(value);
  }

  async register() {
    if(this.registerForm.valid && !this.phoneNumberUsed){
      this.loading = true;
      this.apiRequest.registerByPhoneNumber(this.registerForm.value).then((data) => {
        this.loading = false;
        if(data){
          this.router.navigate(['/index/login']);
        }
      }).catch((e) => {
        this.loading = false;
        if(e.length && e.length !== 0){
          this.error = e[0];
        }
      });
    }
  }

}
