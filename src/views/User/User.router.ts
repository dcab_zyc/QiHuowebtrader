import { Routes } from '@angular/router';
import { UserComponent } from './User.component';

export const ROUTES: Routes = [
  { path: '', component: UserComponent },
  { path: 'editor', loadChildren: 'views/User/UserEditor/UserEditor.module#UserEditorModule'}
];