import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { ROUTES } from './User.router'; 
import { UserComponent } from './User.component';
// import { PopupDirective } from './popup';

@NgModule({
  declarations: [
    UserComponent,
    // PopupDirective
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    CommonModule,
    NgxPaginationModule,
    FormsModule
  ],
  exports: [
  ],
  providers: [
    
  ],
})
export class UserModule {
  constructor() {
    
  }
}