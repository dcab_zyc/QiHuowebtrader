import { Routes } from '@angular/router';

import { UserEditorComponent } from './UserEditor.component';
import { PasswordEditorComponent } from './PasswordEditor/PasswordEditor.component';
import { PersonalDataComponent } from './PersonalData/PersonalData.component';

export const ROUTES: Routes = [
  {
    path: '', component: UserEditorComponent, children: [
      { path: '', component: PersonalDataComponent },
      { path: 'personaldata', component: PersonalDataComponent },
      { path: 'pwdeditor', component: PasswordEditorComponent },
      { path: 'accountbind', loadChildren: './AccountBinding/AccountBinding.module#AccountBindingModule' },
      { path: 'safetyrecords', loadChildren: './SafetyRecords/SafetyRecords.module#SafetyRecordsModule' },
    ]
  }
];
