import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgxPaginationModule } from 'ngx-pagination';

import { ROUTES } from './UserEditor.router';
import { PasswordEditorComponent } from './PasswordEditor/PasswordEditor.component';
import { PersonalDataComponent } from './PersonalData/PersonalData.component';
import { UserEditorComponent } from './UserEditor.component';

@NgModule({
  declarations: [
    PasswordEditorComponent,
    PersonalDataComponent,
    UserEditorComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
  ],
  providers: [
  ]
})
export class UserEditorModule {
  constructor() {

  }
}
