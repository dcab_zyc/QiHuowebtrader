import { Component } from '@angular/core';
import { AuthService } from 'service/AuthService';

@Component({
    selector: 'user-editor',
    templateUrl: './UserEditor.html',
  }
)

export class UserEditorComponent {

  userInfo:any = null;

  hideList = [true, true];

  constructor(private authService: AuthService) {
    this.userInfo = this.authService.getUserInfo();
  }

  ngOnInit() {
  }

  onIconClick(i) {
    this.hideList[i] = !this.hideList[i];
  }

}
