import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ROUTES } from './AccountBinding.router';
import { AccountBindingComponent } from './AccountBinding.component';
import { EmailBindingComponent } from './EmailBinding/EmailBinding.component';
import { EmailValComponent } from './EmailVal/EmailVal.component';
import { PhoneBindingComponent } from './PhoneBinding/PhoneBinding.component';
import { PhoneChangeComponent } from './PhoneChange/PhoneChange.component';

@NgModule({
  declarations: [
    EmailBindingComponent,
    PhoneBindingComponent,
    AccountBindingComponent,
    PhoneChangeComponent,
    EmailValComponent,
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
  ],
  providers: [
  ],
})
export class AccountBindingModule {
  constructor() {

  }
}
