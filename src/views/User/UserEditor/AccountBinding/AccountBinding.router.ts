import { Routes } from '@angular/router';

import { AccountBindingComponent } from './AccountBinding.component';
import { EmailBindingComponent } from './EmailBinding/EmailBinding.component';
import { EmailValComponent } from './EmailVal/EmailVal.component';
import { PhoneBindingComponent } from './PhoneBinding/PhoneBinding.component';
import { PhoneChangeComponent } from './PhoneChange/PhoneChange.component';

export const ROUTES:Routes = [
  {path: '', component: AccountBindingComponent, pathMatch: 'full'},
  {path: 'phoneBind', component: PhoneBindingComponent, },
  {path: 'phoneChange', component: PhoneChangeComponent, },
  {path: 'emailBind', component: EmailBindingComponent, },
  {path: 'emailVal', component: EmailValComponent, }
];
