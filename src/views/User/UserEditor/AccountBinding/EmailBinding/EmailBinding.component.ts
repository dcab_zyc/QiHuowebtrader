import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'news-detail',
    templateUrl: './EmailBinding.html',
  }
)

export class EmailBindingComponent {

  //loading
  private loading: boolean = false;
  //后端错误
  private error: string = '';
  //邮箱
  private Email: string = '';

  public emailBindForm:FormGroup;

  constructor(private formBuilder:FormBuilder, private router: Router, private apiRequest:ApiRequest,private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.Email = data["email"];
    });

    this.emailBindForm = this.formBuilder.group({
      Email: [null, Validators.compose([Validators.required, Validators.email])],
    });
  }

  // 清除输入框内容
  clear(key) {
    const value = this.emailBindForm.value;
    value[key] = '';
    this.emailBindForm.setValue(value);
  }

  async emailBind() {
  }

}
