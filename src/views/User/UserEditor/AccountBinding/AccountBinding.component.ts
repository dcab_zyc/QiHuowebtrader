import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'service/AuthService';

@Component({
    selector: 'news-detail',
    templateUrl: './AccountBinding.html',
  }
)

export class AccountBindingComponent {

  userInfo:any = null;

  constructor(private authService: AuthService) {
    this.userInfo = this.authService.getUserInfo();
  }

  ngOnInit() {

  }

}
