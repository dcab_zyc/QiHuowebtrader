import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { phoneNumberValidator } from 'utils/index';
import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'findback',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './PhoneChange.html',
    styleUrls: []
  }
)

export class PhoneChangeComponent {
  //loading
  private loading: boolean = false;
  //手机号是否被注册
  private phoneNumberUsed: boolean = true;
  //验证码倒计时
  private countTime: number = 0;
  //后端错误
  private error: string = '';
  //手机号
  private PhoneNumber: string = '';
  //下一步
  private next: boolean = false;

  public phoneChangeForm:FormGroup;

  public phoneNewBindForm:FormGroup;

  constructor(private formBuilder:FormBuilder, private router: Router, private apiRequest:ApiRequest,private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.PhoneNumber = data["phoneNumber"];
    });

    this.phoneChangeForm = this.formBuilder.group({
      VerificationCode: [null, Validators.compose([Validators.required])],
    });

    this.phoneNewBindForm = this.formBuilder.group({
      PhoneNumber:[null, Validators.compose([Validators.required, phoneNumberValidator])],
      VerificationCode: [null, Validators.compose([Validators.required])],
    });
  }

  isPhoneNumberUsed(){
    if(!this.phoneChangeForm.controls.PhoneNumber.errors){
      this.apiRequest.isPhoneNumberUsed(this.phoneChangeForm.value.PhoneNumber).then((data) => {
        this.phoneNumberUsed = data;
      });
    }
  }

  // 清除输入框内容
  clear(key) {
    const value = this.phoneChangeForm.value;
    value[key] = '';
    this.phoneChangeForm.setValue(value);
  }

  async phoneChange() {
    this.next = true;
  }

}
