import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';

import { phoneNumberValidator } from 'utils/index';
import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'phone-binding',
    templateUrl: './PhoneBinding.html',
  }
)

export class PhoneBindingComponent {

  //loading
  private loading: boolean = false;
  //手机号是否被注册
  private phoneNumberUsed: boolean = true;
  //验证码倒计时
  private countTime: number = 0;
  //后端错误
  private error: string = '';

  public phoneBindForm:FormGroup;

  constructor(private formBuilder:FormBuilder, private router: Router, private apiRequest:ApiRequest) {
  }

  ngOnInit() {
    this.phoneBindForm = this.formBuilder.group({
      PhoneNumber:[null, Validators.compose([Validators.required, phoneNumberValidator])],
      VerificationCode: [null, Validators.compose([Validators.required])],
    });
  }

  isPhoneNumberUsed(){
    if(!this.phoneBindForm.controls.PhoneNumber.errors){
      this.apiRequest.isPhoneNumberUsed(this.phoneBindForm.value.PhoneNumber).then((data) => {
        this.phoneNumberUsed = data;
      });
    }
  }

  // 清除输入框内容
  clear(key) {
    const value = this.phoneBindForm.value;
    value[key] = '';
    this.phoneBindForm.setValue(value);
  }

  async phoneBind() {
  }

}
