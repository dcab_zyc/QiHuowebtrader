import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'news-detail',
    templateUrl: './EmailVal.html',
  }
)

export class EmailValComponent {

  //邮箱
  private Email: string = '';
  //成功
  private success: boolean = false;

  constructor(private formBuilder:FormBuilder, private router: Router, private apiRequest:ApiRequest,private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.Email = data["email"];
    });
  }


}
