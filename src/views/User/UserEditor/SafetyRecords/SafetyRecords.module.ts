import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgxPaginationModule } from 'ngx-pagination';

import { ROUTES } from './SafetyRecords.router';
import { SafetyRecordsComponent } from './SafetyRecords.component';
import { RecentLoginComponent } from './RecentLogin/RecentLogin.component';
import { HistoryComponent } from './History/History.component';

@NgModule({
  declarations: [
    SafetyRecordsComponent,
    RecentLoginComponent,
    HistoryComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
  ],
  providers: [
  ]
})
export class SafetyRecordsModule {
  constructor() {

  }
}
