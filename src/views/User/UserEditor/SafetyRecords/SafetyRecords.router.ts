import { Routes } from '@angular/router';

import { SafetyRecordsComponent } from './SafetyRecords.component';
import { RecentLoginComponent } from './RecentLogin/RecentLogin.component';
import { HistoryComponent } from './History/History.component';

export const ROUTES: Routes = [
  {
    path: '', component: SafetyRecordsComponent, children: [
      { path: '', component: RecentLoginComponent },
      { path: 'recentLogin', component: RecentLoginComponent },
      { path: 'history', component: HistoryComponent },
    ]
  }
];
