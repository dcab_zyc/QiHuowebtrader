import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'user',
  encapsulation: ViewEncapsulation.Emulated,
  templateUrl: './User.html',
  styleUrls: []
}
)

export class UserComponent {
  //展开和收起S
  private optiontoggle: boolean = false;
  //我的自选分组
  private optinalGroup = [
    {
      name: '全部',
      groupnumber: 0
    }, {
      name: '自选1',
      groupnumber: 1,
    }, {
      name: '自选2',
      groupnumber: 2,
    }, {
      name: '自选3',
      groupnumber: 3
    },
  ];
  //我的自选数据
  private optionalData = [
    {
      'id': 0,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    },
    {
      'id': 1,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    },
    {
      'id': 2,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    }, {
      'id': 3,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    }, {
      'id': 4,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    },
    {
      'id': 5,
      'name': '现货黄金',
      'newprice': 11323.11,
      'updown': '8.21%',
      'openprice': 1130.22,
      'yesprice': 1130.22,
      'heightprice': 1130.22,
      'lowprice': 1130.22
    }
  ];
  //我的收藏
  private collection = [
    {
      'id': 0,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 1,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 2,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 3,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 4,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 5,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 6,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
    {
      'id': 7,
      'title': '美国大都会人寿保险公司大厦',
      'desc': '托尼·斯塔克（Tony Stark）生于纽约长岛，仅十五岁时就进入麻省理工学院电子工程系大学部就读并以最高分毕业。在电影里，二十一岁时他的父亲霍华德·斯塔克死于车祸（其实是有策划的阴谋，在《美国队长3》中说道）。托尼·斯塔克的母亲是玛丽亚·斯塔克（Maria Stark）。斯塔克继承了父亲拥有的斯塔克企业（Stark Industries），并在接管公司后作的第一件事就是买下制造他父母座车上瑕疵刹车系统的公司并修正其设计上的缺陷。在一次前往越南',
      'date': '1493084267640',
      'collectors': 18
    },
  ];

  constructor() {

  }

  ngOnInit() {

  }

  //展开收起
  toggleOptional(flag): void {
    this.optiontoggle = flag;
  }

  //编辑分组的显示与隐藏和点击高亮
  private activeId: number = 0;
  private toggleEditorGroupFlag: boolean = false;
  toggleEditorGroup(id): void {
    if (!id) {
      this.toggleEditorGroupFlag = false;
    } else {
      this.toggleEditorGroupFlag = true;
    }
    this.activeId = id;
  }

  //重命名和删除分组
  private groupname: string;
  private utils = {
    groupRename: false,
    delGroup: false
  }
  switchPrompt(tab, status) {
    this.utils[tab] = status;
  }
  groupRename() {
    console.log(this.groupname);
    this.utils.groupRename = false;
  }
  delGroup() {
    this.utils.delGroup = false;
  }
}