import { Component } from '@angular/core';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import 'moment/locale/zh-cn.js';
import * as moment from 'moment';

import { ApiRequest } from 'service/ApiRequest';
import { COUNTRY, OTHER_COUNTRY } from 'utils/index';

@Component({
    selector: 'economic-calendar',
    templateUrl: './EconomicCalendar.html',
  }
)

export class EconomicCalendarComponent {

  date: DateModel;
  options: DatePickerOptions;
  weekList = [];
  eventList = [];
  weekdays = ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"];

  countryList = Object.assign({}, COUNTRY, OTHER_COUNTRY);
  
  constructor(private apiRequest:ApiRequest) {
    
  }

  ngOnInit() {

    console.log(this.countryList);

    moment.locale('zh-cn');
    this.weekList = this.initWeekList(moment().valueOf());
    this.options = new DatePickerOptions({
      autoApply: false,
      style: 'big',
      locale: 'zh-cn',
      maxDate: new Date(moment().format('YYYY-MM-DD')),
      initialDate: new Date(moment().format('YYYY-MM-DD')),
      firstWeekdaySunday: false,
      format: 'YYYY 年 MM 月 DD 日',
      selectYearText: '请选择日期',
      todayText: '今天',
      clearText: '清除'
    });

    const eTime = moment(moment().format('YYYY-MM-DD 23:59:59')).unix();
    const sTime = moment(moment().format('YYYY-MM-DD 00:00:00')).unix();
    this.loadData(sTime, eTime);
  }

  onDateChange(time) {
    if (time.type === "dateChanged") {
      console.log(time);
      const eTime = moment(time.data.momentObj.format('YYYY-MM-DD 23:59:59')).unix();
      const sTime = moment(time.data.momentObj.format('YYYY-MM-DD 00:00:00')).unix();
      this.loadData(sTime, eTime);
      this.weekList = this.initWeekList(time.data.momentObj.valueOf());
    }
  }

  initWeekList(sTime) {
    const tempList = [];
    for (let i=0; i < 7; i++) {
      const minDate = moment(sTime).startOf('week');
      tempList.push(minDate.add(i, 'days'));
    }

    return tempList;
  }

  caculate(date = null) {
    if (date) {
      return moment(date).format('YYYY-MM-DD');
    }

    return moment().format('YYYY-MM-DD');
  }

  nextWeek() {
    if (this.weekList[6] && this.weekList[6].valueOf() < moment().valueOf()) {
      const tagTime = moment(this.weekList[6].valueOf()).add(7, 'days');
      this.weekList = this.initWeekList(tagTime);
      this.date = new DateModel({
        momentObj: this.weekList[0],
        day: this.weekList[0].format('DD'),
        year: this.weekList[0].format('YYYY'),
        month: this.weekList[0].format('MM'),
        formatted: this.weekList[0].format('YYYY 年 MM 月 DD 日')
      });
    }

    const eTime = moment(this.weekList[0].format('YYYY-MM-DD 23:59:59')).unix();
    const sTime = moment(this.weekList[0].format('YYYY-MM-DD 00:00:00')).unix();
    this.loadData(sTime, eTime);
  }

  preWeek() {
    if (this.weekList[0] && this.weekList[0].valueOf() > 0) {
      const tagTime = moment(this.weekList[0].valueOf()).subtract(7, 'days');
      this.weekList = this.initWeekList(tagTime);
      this.date = new DateModel({
        momentObj: this.weekList[6],
        day: this.weekList[6].format('DD'),
        year: this.weekList[6].format('YYYY'),
        month: this.weekList[6].format('MM'),
        formatted: this.weekList[6].format('YYYY 年 MM 月 DD 日')
      });
    }

    const eTime = moment(this.weekList[6].format('YYYY-MM-DD 23:59:59')).unix();
    const sTime = moment(this.weekList[6].format('YYYY-MM-DD 00:00:00')).unix();
    this.loadData(sTime, eTime);
  }

  changeDate(data) {
    if (this.caculate(data) <= this.caculate()) {
      this.date = new DateModel({
        momentObj: data,
        day: data.format('DD'),
        year: data.format('YYYY'),
        month: data.format('MM'),
        formatted: data.format('YYYY 年 MM 月 DD 日')
      });

      const eTime = moment(data.format('YYYY-MM-DD 23:59:59')).unix();
      const sTime = moment(data.format('YYYY-MM-DD 00:00:00')).unix();
      this.loadData(sTime, eTime);
    }
  }

  loadData(sTime, eTime) {
    this.apiRequest.getCalendarReserveOrder("cd", sTime, eTime).then((data) => {
      this.eventList = data.map((item) => {
        item.Time = this.timeConvert(item.Time);
        return item;
      });
    });
  }

  timeConvert(time) {
    return moment(time * 1000).format("HH:mm");
  }
}