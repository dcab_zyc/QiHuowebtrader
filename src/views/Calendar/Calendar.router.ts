import { Routes } from '@angular/router';
import { CalendarComponent } from './Calendar.component';
import { CountryCalendarComponent } from './CountryCalendar/CountryCalendar.component';
import { EconomicCalendarComponent } from './EconomicCalendar/EconomicCalendar.component';

export const ROUTES: Routes = [
  { path: '', component: CalendarComponent, children: [
    {path: '', redirectTo: 'econonmic', pathMatch: 'full'},
    {path: 'country/:id', component: CountryCalendarComponent},
    {path: 'econonmic', component: EconomicCalendarComponent}
  ]}
];