import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DatePickerModule } from 'ng2-datepicker';

import { ROUTES } from './Calendar.router'; 
import { CalendarComponent } from './Calendar.component';
import { CountryCalendarComponent } from './CountryCalendar/CountryCalendar.component';
import { EconomicCalendarComponent } from './EconomicCalendar/EconomicCalendar.component';
import { DataTimeDirective } from 'directive/DataTime.directive';


@NgModule({
  declarations: [
    CalendarComponent,
    CountryCalendarComponent,
    EconomicCalendarComponent,
    DataTimeDirective
  ],
  imports:[
    RouterModule.forChild(ROUTES),
    CommonModule,
    DatePickerModule
  ],
  exports: [
  ],
  providers: [
    
  ],
})
export class CalendarModule {
  constructor() {
    
  }
}