import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiRequest } from 'service/ApiRequest';
import * as moment from 'moment';
import { COUNTRY } from 'utils/index';

const CD_USD = {
  CategoryId: 'cd_USD',
  Name: '美国',
  IconUrl: 'country_flag_usa.png'
};

@Component({
    selector: 'country-calendar',
    templateUrl: './CountryCalendar.html',
  }
)

export class CountryCalendarComponent {

  public hideList = true;

  country;
  eventList = [];

  constructor(private route: ActivatedRoute, private apiRequest:ApiRequest) {

  }

  ngOnInit() {
    this.eventList = [];
    this.route.params.subscribe((params) => {

      if (params["id"] == "cd_USD") {
        this.country = CD_USD;
      } else {
        this.country = COUNTRY[params["id"]];
      }

      const start = moment(moment().format("YYYY-MM-DD 00:00:00")).unix();;
      const end = moment(moment().format("YYYY-MM-DD 23:59:59")).unix();

      this.apiRequest.getCalendarReserveOrder(this.country.CategoryId, start, end).then((data) => {
        this.eventList = data.map((item) => {
          item.Time = this.timeConvert(item.Time);
          return item;
        });
      });
    });
  }


  timeConvert(time) {
    return moment(time * 1000).format("HH:mm");
  }
}