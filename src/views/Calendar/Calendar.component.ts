import { Component } from '@angular/core';
import { ApiRequest } from 'service/ApiRequest';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'calendar',
    templateUrl: './Calendar.html',
  }
)

export class CalendarComponent {

  hideList = true;
  category;

  constructor(private apiRequest:ApiRequest, private title:Title) {

  }

  ngOnInit() {
    this.title.setTitle("财经日历");
     this.apiRequest.getCalendarMeta().then((data) => {
       this.category = data;
     });
  }

  onIconClick() {
    this.hideList = !this.hideList;
  }

}