import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { ROUTES } from './Market.router';
import { MarketComponent } from './Market.component';
import { SignalRService } from 'service/SignalRService';

import { HttpService } from 'apirequest_yeez/ApiRequest/Common/Services/HTTPService';
import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';

@NgModule({
  declarations: [
    MarketComponent
  ],
  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(ROUTES),
  ],
  exports: [
  ],
  providers: [
    HttpService,
    ApiRequest
  ],
  bootstrap: [
  ]
})

export class MarketModule {
  constructor() {

  }
}