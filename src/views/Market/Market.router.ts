import { Routes } from '@angular/router';
import { MarketComponent } from './Market.component';

export const ROUTES: Routes = [
  { path: '', component: MarketComponent },
  { path: ':id', loadChildren: 'views/WebTrader_Trade/MarketDetail/MarketDetail.module#MarketDetailModule' }
];