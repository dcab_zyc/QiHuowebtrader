import { Injectable } from '@angular/core';
import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';
import { Provider } from 'domain/Provider';
import { Product } from 'domain/Product';
import { ProductMeta } from 'domain/ProductMeta';
@Injectable()
export class MarketService {
  apiToUrl: any;
  constructor(private apiRequest: ApiRequest) { }

  //设置基本参数
  setParams(HTTP_URL: string, BASE_API: string) {
    this.apiRequest.setInitializationOption(HTTP_URL, BASE_API);
  }

  //初始化
  async init(): Promise<boolean> {
    let flag: boolean;
    await this.apiRequest
      .init()
      .then(res => {
        flag = res;
        this.apiToUrl = this.apiRequest.apiToUrl;
      })
      .catch(err => {
        throw new Error(err);
      });
    return flag;
  }

  //获取productmeta
  async getProductMeta(): Promise<ProductMeta[]> {
    let productMetas: ProductMeta[] = [];
    await this.apiRequest.QHQuoteApiRequest.getProductMeta().then(res => {
      productMetas = res;
    }).catch(err => {
      throw new Error(err);
    })
    return productMetas;
  }
}