/**
 * 行情詳情組件下四個子組件
 * 1 chart圖表
 * 2 實時行情
 * 3 持倉和委托
 * 4 下單
 */

//vm interface
//行情詳情
// import { SignalRService } from 'service/SignalRService';

interface MarketDetail {
  //持仓量
  openInterest: number;
  //成交量
  volume: number;
  //日增量
  dailyIncrement: number;
  //交易所
  providers: {
    BindedTradeCode: string,
    IconUrl: string,
    Id: string,
    Name: string,
    OpenAccountUrl: string,
  }[];
  //交易品
  products: {
    Id: string,
    Name: string,
    MarketSymbol: string,
    DescriptionUrl: string,
    UnitPerLot: number,
    TradableTime: {
      Ranges: {
        StartTimeUtc: number,
        EndTimeUtc: number
      }[],
      PublicHolidays: number[]
    }
  }[];
  //交易参数
  tradeParams: {
    MaxLot: number,
    MinLot: number,
    BuyMargin: number,
    SellMargin: number,
    BuyMarginType: number,
    SellMarginType: number,
    CloseCommissionType: number,
    OpenCommissionType: number,
    OpenCommission: number,
    CloseCommission: number,
    PriceStep: number,
  }

  //商品成交记录
  realTimeList: {}[],

  //元信息
  quoteBasic: {
    Symbol: string,
    Time: number,
    Volume: string,
    YesterdayClose: number,
    DayOpen: number,
    DayHigh: number,
    DayLow: number,
    CurrentPrice: number,
    BuyPrice: number,
    SellPrice: number,
    floating: number,
    percent: number
  },

  //signalRService: SignalRService;

  //用户信息
  userInfo: {
    TradeCode: string,//username
    fund: {//资金信息
      Avaliable: number,
      Withdrawable: number,
      FrozenMargin: number,
      FrozenCommission: number,
      HoldMargin: number,
    },
    order: {//委托单信息
      OrderId: string,
      Product: {
        Id: string,
        Name: string,
        MarketSymbol: string,
        DescriptionUrl: string,
        UnitPerLot: number,
        TradableTime: {
          Ranges: {
            StartTimeUtc: number,
            EndTimeUtc: number
          }[],
          PublicHolidays: number[]
        }
      },
      OpenClose: number,
      BuySell: number,
      OrderStatus: number,
      OrderTime: number,
      OrderLot: number,
      OrderPrice: number,
      TradedLot: number
    }[],
    hold: {//持仓单信息
      OrderId: string,
      Product: {
        Id: string,
        Name: string,
        MarketSymbol: string,
        DescriptionUrl: string,
        UnitPerLot: number,
        TradableTime: {
          Ranges: {
            StartTimeUtc: number,
            EndTimeUtc: number,
          }[],
          PublicHolidays: number[]
        }
      },
      BuySell: number,
      HoldLot: number,
      OpenPrice: number,
      TodayHold: number,
      YesterdayHold: number,
    }[],
  },

  //找到symbol对应的category
  loadCategory(): void;

  //获取用户资金信息
  getUserInfo(providerId: string): void;

  //获取交易参数信息
  getTradeParameter(providerId: string, ProductId: string): void;
}


//图表
interface Chart {
  loadData(symbol: string, interval: number, sTime: number, eTime: number, maxCount: number): void;
  onRangeChange(index: number): void;
  onStatShow(e: Event): void;
  showMACDStat(): void;
  showBollStat(): void;
  showRSIStat(): void;
  showBIASStat(): void;
  showMAStat(): void;
  showEMAStat(): void;
  onBlur(e: Event, cate: string, type: string): void;
  addGrid(name: string, data: number[]): void;
  removeGrid(name: string): void;
  recacuChart(): void;
  addLines(name: string): void;
  removeLines(name: string): void;
  updateLines(name: string): void;
  updatePoint(): void;
  resetConfig(): void;
  combineLine(name, type, data, color, index: number | null): void;
}

//委托持仓
interface HAO {
  order: {//委托单信息
    OrderId: string,
    Product: {
      Id: string,
      Name: string,
      MarketSymbol: string,
      DescriptionUrl: string,
      UnitPerLot: number,
      TradableTime: {
        Ranges: {
          StartTimeUtc: number,
          EndTimeUtc: number
        }[],
        PublicHolidays: number[]
      }
    },
    OpenClose: number,
    BuySell: number,
    OrderStatus: number,
    OrderTime: number,
    OrderLot: number,
    OrderPrice: number,
    TradedLot: number
  }[],
  hold: {//持仓单信息
    OrderId: string,
    Product: {
      Id: string,
      Name: string,
      MarketSymbol: string,
      DescriptionUrl: string,
      UnitPerLot: number,
      TradableTime: {
        Ranges: {
          StartTimeUtc: number,
          EndTimeUtc: number,
        }[],
        PublicHolidays: number[]
      }
    },
    BuySell: number,
    HoldLot: number,
    OpenPrice: number,
    TodayHold: number,
    YesterdayHold: number,
  }[],
  //撤单
  revokeOrder(ProviderId: string, ProductId: string, OrderId: string, Password: string): void;
  //平仓
  closePosition(): void;
}

//交易
interface Trade {
  avaliable: number;//可用资金
  symbol: string;
  minPrice: number;//最小变动价格
  maxLots: number;//最大可开仓手数
  longOnlyFunds: {//买多
    price: number;
    lots: number;
  }
  shortSalesFunds: {//卖空
    price: number;
    lots: number;
  }

  //买多
  longing(): void;

  //卖空
  shortting(): void;
}

//dm interface
//行情详情
interface DmMarketDetail {

  //获取所有期货提供商
  getProviders(): Promise<{
    BindedTradeCode: string,
    IconUrl: string,
    Id: string,
    Name: string,
    OpenAccountUrl: string,
  }[]>;

  //获取期货下所有商品
  getProducts(providerId: string): Promise<{
    Id: string,
    Name: string,
    MarketSymbol: string,
    DescriptionUrl: string,
    UnitPerLot: number,
    TradableTime: {
      Ranges: {
        StartTimeUtc: number,
        EndTimeUtc: number
      }[],
      PublicHolidays: number[]
    }
  }[]>;

  //获取用户信息
  getUserInfo(providerId: string): Promise<{
    TradeCode: string,//username
    fund: {//资金信息
      Avaliable: number,
      Withdrawable: number,
      FrozenMargin: number,
      FrozenCommission: number,
      HoldMargin: number,
    },
    order: {//委托单信息
      OrderId: string,
      Product: {
        Id: string,
        Name: string,
        MarketSymbol: string,
        DescriptionUrl: string,
        UnitPerLot: number,
        TradableTime: {
          Ranges: {
            StartTimeUtc: number,
            EndTimeUtc: number
          }[],
          PublicHolidays: number[]
        }
      },
      OpenClose: number,
      BuySell: number,
      OrderStatus: number,
      OrderTime: number,
      OrderLot: number,
      OrderPrice: number,
      TradedLot: number
    }[],
    hold: {//持仓单信息
      OrderId: string,
      Product: {
        Id: string,
        Name: string,
        MarketSymbol: string,
        DescriptionUrl: string,
        UnitPerLot: number,
        TradableTime: {
          Ranges: {
            StartTimeUtc: number,
            EndTimeUtc: number,
          }[],
          PublicHolidays: number[]
        }
      },
      BuySell: number,
      HoldLot: number,
      OpenPrice: number,
      TodayHold: number,
      YesterdayHold: number,
    }[],
  }>;

  //获取交易参数信息
  getTradeParameter(
    providerId: string,
    ProductId: string
  ): Promise<{}>;
}

//委托和持仓
interface DmHAO {
  //撤单
  revokeOrder(
    ProviderId: string,
    ProductId: string,
    OrderId: string,
    Password: string
  ): Promise<any>;
  //平仓
  closePosition(
    ProviderId: string,
    TradeProductId: string,
    Price: number,
    Lot: number,
    OpenClose: number,
    BuySell: number,
    Password: string,
    TodayHold: number,
    YesterdayHold: number,
  ): Promise<any>;
}

//交易
interface DmTrade {
  // 买多
  longing(
    ProviderId: string,
    TradeProductId: string,
    Price: number,
    Lot: number,
    OpenClose: number,
    BuySell: number,
    Password: string,
    TodayHold: number,
    YesterdayHold: number,
  ): Promise<any>;
  // 卖空
  shortting(
    ProviderId: string,
    TradeProductId: string,
    Price: number,
    Lot: number,
    OpenClose: number,
    BuySell: number,
    Password: string,
    TodayHold: number,
    YesterdayHold: number,
  ): Promise<any>;
}

//图表
//...