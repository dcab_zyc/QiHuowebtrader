import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'market-detail',
    templateUrl: './BigMarketDetail.html'
  }
)
export class BigMarketDetailComponent {

  symbol = '';

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {
     this.route.params.subscribe((params:Params) => {
       console.log(params);
       this.symbol = params['id'];
     });
  }
}