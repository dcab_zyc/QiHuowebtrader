export interface IMarketDetail {
  symbol: string;
  category: string;
  realTimeList: any[];
  symbolHistory: any[];

  //判断用户是否已登录
  userIsLogin: boolean;

}