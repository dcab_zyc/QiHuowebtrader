export interface IMarketDetail {
  //持仓量
  openInterest: number;
  //成交量
  volume: number;
  //日增量
  dailyIncrement: number;
  //交易所
  providers: {
    BindedTradeCode: string,
    IconUrl: string,
    Id: string,
    Name: string,
    OpenAccountUrl: string,
  }[];
  //交易品
  products: {
    Id: string,
    Name: string,
    MarketSymbol: string,
    DescriptionUrl: string,
    UnitPerLot: number,
    TradableTime: {
      Ranges: {
        StartTimeUtc: number,
        EndTimeUtc: number
      }[],
      PublicHolidays: number[]
    }
  }[];
  //商品成交记录
  realTimeList: {}[],
  //元信息
  quoteBasic: {
    Symbol: string,
    Time: number,
    Volume: string,
    YesterdayClose: number,
    DayOpen: number,
    DayHigh: number,
    DayLow: number,
    CurrentPrice: number,
    BuyPrice: number,
    SellPrice: number,
    floating: number,
    percent: number
  },
  //用户信息
  userInfo: {
    TradeCode: string,//username
    fund: {//资金信息
      Avaliable: number,
      Withdrawable: number,
      FrozenMargin: number,
      FrozenCommission: number,
      HoldMargin: number,
    }
  }
  //找到symbol对应的category
  loadCategory(): void;
  //获取用户信息
  getUserInfo(providerId: string): void;
}