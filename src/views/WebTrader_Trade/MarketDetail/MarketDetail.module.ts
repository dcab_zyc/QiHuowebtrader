import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { EchartsNg2Module } from 'echarts-ng2';
import { FormsModule } from '@angular/forms';

import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';
import { HttpService } from 'apirequest_yeez/ApiRequest/Common/Services/HTTPService';

import { ROUTES } from './MarketDetail.router';
import { MarketDetailComponent } from './MarketDetail.component';
import { MarketDetailService } from './MarketDetail.service';
import { BigMarketDetailComponent } from './BigMarketDetail/BigMarketDetail.component';
import { TransactionDetailsComponent } from './TransactionDetails/TransactionDetails.component';
import { PersonalTransactionComponent } from './PersonalTransaction/PersonalTransaction.component';
import { EntrustAndPositionComponent } from './EntrustAndPosition/EntrustAndPosition.component';
import { AllProvidersProductsComponent } from './AllProvidersProducts/AllProvidersProducts.component';
import { ChartModule } from "components/Chart/Chart.module";

@NgModule({
  declarations: [
    MarketDetailComponent,
    BigMarketDetailComponent,
    TransactionDetailsComponent,
    PersonalTransactionComponent,
    EntrustAndPositionComponent,
    AllProvidersProductsComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    EchartsNg2Module,
    ChartModule
  ],
  exports: [
  ],
  providers: [
    MarketDetailService,
    ApiRequest,
    HttpService
  ],
})
export class MarketDetailModule {
  constructor() {

  }
}