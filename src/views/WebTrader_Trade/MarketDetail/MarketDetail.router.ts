import { Routes } from '@angular/router';
import { MarketDetailComponent } from './MarketDetail.component';
import { BigMarketDetailComponent } from './BigMarketDetail/BigMarketDetail.component';

export const ROUTES: Routes = [
  { path: '', component: MarketDetailComponent },
  { path: 'big', component: BigMarketDetailComponent },
];