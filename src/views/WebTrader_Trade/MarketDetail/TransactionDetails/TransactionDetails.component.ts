import { Component, ViewChild, Input, ElementRef, Renderer2, EventEmitter } from '@angular/core';
import { TransactionService } from './Transaction.Service';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './TransactionDetails.html',
  providers: [TransactionService]
})
export class TransactionDetailsComponent {
  @Input('realTimeList') realTimeList: any[];
  @Input('bottomposition') bottomposition: Element;
  @ViewChild('ch') ch: ElementRef;
  constructor(private r2: Renderer2, private tService: TransactionService) {
  }
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.sendEle();
  }

  sendEle() {
    let t = setInterval(() => {
      if (this.bottomposition) {
        clearInterval(t);
        this.tService.fixPocation(this.ch, this.bottomposition);
      }
    }, 100);
  }
}