import { Injectable, ElementRef, Renderer2 } from '@angular/core';

@Injectable()
export class TransactionService {
  constructor(private r2: Renderer2) {

  }

  //设置高度
  fixPocation(pannel: ElementRef, bottom: Element) {
    let minHeight = parseInt($(pannel.nativeElement).css('min-height'));
    $(window).on('resize', () => {
      // console.log($(document.body).height() - $(bottom).height() - 50 - 70 - 10 - 20 - 6);
      let ch = $(document.body).height() - $(bottom).height() - 50 - 70 - 10 - 20 - 6;
      this.r2.setStyle(pannel.nativeElement, 'height', `${ch}px`);
      if (ch <= minHeight) {
        $(bottom).css('position','static');
      } else {
        $(bottom).css('position','fixed');
      }
    }).trigger('resize');
  }
}