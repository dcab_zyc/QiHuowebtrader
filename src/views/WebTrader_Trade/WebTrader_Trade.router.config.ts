import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  { path: ':id', loadChildren: './MarketDetail/MarketDetail.module#MarketDetailModule' },
];
