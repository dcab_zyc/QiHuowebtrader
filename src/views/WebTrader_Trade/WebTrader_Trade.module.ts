import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DatePickerModule } from 'ng2-datepicker';
import { HttpModule } from '@angular/http';
import { ROUTES } from './WebTrader_Trade.router.config';

import { HttpService } from 'apirequest_yeez/ApiRequest/Common/Services/HTTPService';
import { ApiRequest } from 'apirequest_yeez/ApiRequest/QH/Services/ApiRequest';

@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
    DatePickerModule,
    HttpModule
  ],
  exports: [
  ],
  providers: [
    HttpService,
    ApiRequest
  ],
})
export class WebTraderTradeModule {
  constructor() { }
}