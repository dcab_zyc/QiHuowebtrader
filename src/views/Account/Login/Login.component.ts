import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'account-login',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './Login.html',
    styleUrls: []
  }
)

export class LoginComponent {

  constructor(private apiRequest:ApiRequest) {

  }

  ngOnInit() {
  }

}
