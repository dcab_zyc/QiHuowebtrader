import { Routes } from '@angular/router';

import { ApiCanLoad } from 'service/ApiCanLoad';
import { AuthGuard } from "service/AuthGuard";

import { PersonalComponent } from './Personal.component';

export const ROUTES: Routes = [
  { path: '', component: PersonalComponent }
];
