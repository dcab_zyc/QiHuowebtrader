import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PersonalComponent } from './Personal.component';
import { ROUTES } from './Personal.router';

@NgModule({
  declarations: [
    PersonalComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
  ],
  exports: [
  ],
  providers: [

  ],
})
export class PersonalModule {
  constructor() {
  }
}
