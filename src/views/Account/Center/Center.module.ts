import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CenterComponent } from './Center.component';
import { ROUTES } from './Center.router';

@NgModule({
  declarations: [
    CenterComponent
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule,
  ],
  exports: [
  ],
  providers: [

  ],
  bootstrap:[CenterComponent]
})
export class CenterModule {
  constructor() {
  }
}
