import { Routes } from '@angular/router';

import { ApiCanLoad } from 'service/ApiCanLoad';
import { AuthGuard } from "service/AuthGuard";

import { CenterComponent } from './Center.component';

export const ROUTES: Routes = [
  {
    path: '', component: CenterComponent, children: [
      { path: '',  loadChildren: 'views/Account/Center/Personal/Personal.module#PersonalModule' },
    ]
  },
];
