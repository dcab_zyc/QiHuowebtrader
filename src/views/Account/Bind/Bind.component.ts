import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiRequest } from 'service/ApiRequest';

@Component({
    selector: 'account-bind',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './Bind.html',
    styleUrls: []
  }
)

export class BindComponent {

  constructor(private apiRequest:ApiRequest) {

  }

  ngOnInit() {
  }

}
