import { Routes } from '@angular/router';
import { LoginComponent } from './Login/Login.component';
import { BindComponent } from './Bind/Bind.component';

export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'bind', component: BindComponent },
  { path: 'center', loadChildren: "views/Account/Center/Center.module#CenterModule" }
];
