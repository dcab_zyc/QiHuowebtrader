import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ROUTES } from './Account.router';
import { LoginComponent } from './Login/Login.component';
import { BindComponent } from './Bind/Bind.component';


@NgModule({
  declarations: [
    LoginComponent,
    BindComponent
  ],
  imports:[
    CommonModule,
    RouterModule.forChild(ROUTES),
  ],
  exports: [
  ],
  providers: [

  ],
})
export class AccountModule {
  constructor() {

  }
}
