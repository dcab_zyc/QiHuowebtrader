export class Provider {
  BindedTradeCode: string;
  IconUrl: string;
  Id: string;
  Name: string;
  OpenAccountUrl: string;
}