export class Product {
  Id: string;
  Name: string;
  MarketSymbol: string;
  DescriptionUrl: string;
  UnitPerLot: number;
}