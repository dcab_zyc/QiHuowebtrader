export class ProductMeta {
  Symbol: string;
  SymbolDisplay: string;
  Category: string;
  CategoryDisplay: string;
}