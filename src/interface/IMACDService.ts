import { QuoteBasic } from 'service/QuoteBasic';

export interface IMACDService {

  compute(quote:QuoteBasic, lday:number, sday:number, mday:number):Array<any>;

  update(quote:QuoteBasic, lday:number, sday:number, mday:number, difList:Array<number>, deaList:Array<number>, macdList:Array<number>):Array<any>;
}