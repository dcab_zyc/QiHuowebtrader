import { QuoteBasic } from 'service/QuoteBasic';

export interface ICandleService {
  compute(quote:QuoteBasic):Array<any>;
}