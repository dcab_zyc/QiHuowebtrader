import { Injectable } from '@angular/core';
import { QuoteBasic } from 'service/QuoteBasic';

export interface IEMAService {

  compute(quote:QuoteBasic, day:number):Array<number>;

  update(quote:QuoteBasic, day:number, oldList:Array<number>):Array<number>;
}