import { QuoteBasic } from 'service/QuoteBasic';

export interface IRSIService {

  compute(quote:QuoteBasic, day:number):Array<number>;

  update(quote:QuoteBasic, day:number, oldList:Array<number>):Array<number>;
}