import { QuoteBasic } from 'service/QuoteBasic';

export interface IApiRequest {
  apiToUrl: Map<string, string>;
  baseurl: string;
  authUrl: string;

  init():Promise<boolean>;

  getHistoricalQuote(symbol:string, interval:number, sTime:number, eTime:number, maxCount:number):Promise<QuoteBasic>;

}