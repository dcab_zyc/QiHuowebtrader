import { QuoteBasic } from 'service/QuoteBasic';
import { Observable } from 'rxjs/Observable';

export interface IStatService {

  getHistoricalQuote(symbol:string, interval:number, sTime:number, eTime:number, maxCount):Promise<QuoteBasic>;

  getCandleData(quote:QuoteBasic):Array<number>;

  getMAData(quote:QuoteBasic, day:number):Array<number>;

  updateMAData(quote:QuoteBasic, day:number, oldList:Array<number>):Array<number>;

  getBollData(quote:QuoteBasic, day:number, k:number):Array<any>;

  updateBollData(quote:QuoteBasic, day:number, k:number, mdList:Array<number>, upList:Array<number>, downList:Array<number>):Array<any>;

  getRSIData(quote:QuoteBasic, day:number):Array<number>;

  getBIASData(quote:QuoteBasic, day:number):Array<number>;

  updateBIASData(quote:QuoteBasic, day:number, oldList:Array<number>):Array<number>;

  getEMAData(quote:QuoteBasic, day:number):Array<number>;

  updateEMAData(quote:QuoteBasic, day:number, oldList:Array<number>):Array<number>;

  getMACDData(quote:QuoteBasic, lday:number, sday:number, mday:number):Array<any>;

  updateMACDDAta(quote:QuoteBasic, lday:number, sday:number, mday:number, difList:Array<number>, deaList:Array<number>, macdList:Array<number>):Array<any>;
}