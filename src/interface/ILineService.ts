import { Injectable } from '@angular/core';
import { QuoteBasic } from 'service/QuoteBasic';

export interface ILineService {

  compute(quote:QuoteBasic):Array<any>;

  update(quote:QuoteBasic, oldList:Array<number>):Array<any>;
}