export interface IQuoteBasic {
  symbol:string;
  interval:number;
  time:Array<number>;
  open:Array<number>;
  low:Array<number>;
  high:Array<number>;
  close:Array<number>;

  append(symbol, time, price):boolean;
}