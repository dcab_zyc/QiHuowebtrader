import { QuoteBasic } from 'service/QuoteBasic';

export interface IBollService {
  compute(quote:QuoteBasic, day:number, k:number):Array<any> ;
  update(quote:QuoteBasic, day:number, k:number, mdList:Array<number>, upList:Array<number>, downList:Array<number>):Array<any>;
}