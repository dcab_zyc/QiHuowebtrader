import * as jwt from 'jwt-simple';
import { FormControl, FormGroup } from '@angular/forms';
// 数据缓存
const HASE_CODE = "LKASDKJASKDFKJSDFAHKJSDFHAK";
export const getLocalItem = (item) => {
  let value = localStorage.getItem(item);
  return value ? jwt.decode(value, HASE_CODE) : '';
};

export const setLocalItem = (key, value) => {
  let hash = jwt.encode(value, HASE_CODE);
  localStorage.setItem(key, hash);
}

export const removeItem = (key) => {
  localStorage.removeItem(key);
}

export const HTTP_SUCCESS = 10001;            // http请求服务端错误

export const STAT_RANGE = [{
	show: '1分',
	interval: 60,
	range: 60 * 60 * 3 * 1000,
},{
	show: '5分',
	interval: 5*60,
	range: 60 * 60 * 12 * 1000,
},{
	show: '15分',
	interval: 15*60,
	range: 60 * 60 * 18 * 1000,
},{
	show: '30分',
	interval: 30*60,
	range: 60 * 60 * 24 * 1000,
},{
	show: '60分',
	interval: 60*60,
	range: 60 * 60 * 24 * 15 * 1000,
},{
	show: '日线',
	interval: 24*60*60,
	range: 60 * 60 * 24 * 300 * 1000,
},{
	show: '周线',
	interval: 7*24*60*60,
	range: 60 * 60 * 24 * 365 * 5 * 1000,
},{
	show: '月线',
	interval: 28*24*60*60,
	range: 60 * 60 * 24 * 365 * 10 * 1000,
}];

export const MACD_STAT = {
	short: {
		value: 12,
		min: 2,
		max: 200
	},
	long: {
		value: 26,
		min: 2,
		max: 200
	},
	m: {
		value: 9,
		min: 2,
		max: 200
	}
};

export const KDJ_STAT = {
	length: {
		value: 9,
		min: 2,
		max: 200
	},
	k0: {
		value:3,
		min: 2,
		max: 30
	},
	k1: {
		value:3,
		min: 2,
		max: 30
	}
};

export const BOLL_STAT = {
	stand: {
		value: 20,
		min: 5,
		max: 300
	},
	width: {
		value: 2,
		min: 1,
		max: 10
	}
};

export const EMA_STAT = {
	ema: {
		value: 10,
		min: 2,
		max: 100
	}
};

export const RSI_STAT = {
	rsi1: {
		value: 6,
		min: 2,
		max: 250
	},
	rsi2: {
		value: 12,
		min: 2,
		max: 250
	},
	rsi3: {
		value: 24,
		min: 2,
		max: 250
	}
};

export const BIAS_STAT = {
	bias1: {
		value: 6,
		min: 2,
		max: 250
	},
	bias2: {
		value: 12,
		min: 2,
		max: 250
	},
	bias3: {
		value: 24,
		min: 2,
		max: 250
	}
};

export const MA_STAT = {
	ma: {
		value: 5,
		min: 1,
		max: 250
	}
};

export const MACD_COLOR = {
	macd: "#787d82",
	dif: "#ff7b12",
	dea: "#000"
};

export const BOLL_COLOR = {
	low: "#F56E8D",
	mid: "#60D4AE",
	up: "#DCFA70"
};

export const RSI_COLOR = {
	rsi1: "#64AAD0",
	rsi2: "#FFD973",
	rsi3: "#FF9273"
};

export const BIAS_COLOR = {
	bias1: "#33CCCC",
	bias2: "#FF7373",
	bias3: "#C9F76F"
};

export const MA_COLOR = {
	ma: "#89EC6A"
};

export const EMA_COLOR = {
	ema: "#46A229",
};

export const CANDLE_COLOR = {
	color: "#f04b5b",
	color0: "#2bbe65",
};

export const LINE_COLOR = {
	line: "#689CD2"
};

export const HTTP_URL = "http://114.55.146.142:6004";
// export const HTTP_URL = "http://116.62.53.15:6004";

export const AUTH_URL = "http://114.55.146.142:6004";
// export const AUTH_URL = "http://116.62.53.15:8000";

export const BASE_API = "/api/data/GetApiUrl";


export const COUNTRY = {
		cd_china:	{
			"CategoryId": "cd_china",
			"Name": "中国",
			"IconUrl": "country_flag_china.png"
		},
    cd_usa:  {
			"CategoryId": "cd_usa",
			"Name": "美国",
			"IconUrl": "country_flag_usa.png"
		},
    cd_germany: {
			"CategoryId": "cd_germany",
			"Name": "德国",
			"IconUrl": "country_flag_germany.png"
		},
    cd_japan: {
			"CategoryId": "cd_japan",
			"Name": "日本",
			"IconUrl": "country_flag_japan.png"
		},
    cd_uk: {
			"CategoryId": "cd_uk",
			"Name": "英国",
			"IconUrl": "country_flag_uk.png"
		},
    cd_europeanunion: {
			"CategoryId": "cd_europeanunion",
			"Name": "欧元区",
			"IconUrl": "country_flag_europeanunion.png"
		},
    cd_australia: {
			"CategoryId": "cd_australia",
			"Name": "澳大利亚",
			"IconUrl": "country_flag_australia.png"
		},
    cd_canada:{
        "CategoryId": "cd_canada",
        "Name": "加拿大",
        "IconUrl": "country_flag_canada.png"
      },
    cd_switzerland: {
        "CategoryId": "cd_switzerland",
        "Name": "瑞士",
        "IconUrl": "country_flag_switzerland.png"
      },
    cd_hongkong: {
			"CategoryId": "cd_hongkong",
			"Name": "香港",
			"IconUrl": "country_flag_hongkong.png"
		},
    cd_france: {
			"CategoryId": "cd_france",
			"Name": "法国",
			"IconUrl": "country_flag_france.png"
		},
    cd_singapore: {
			"CategoryId": "cd_singapore",
			"Name": "新加坡",
			"IconUrl": "country_flag_singapore.png"
		},
    cd_newzealand: {
			"CategoryId": "cd_newzealand",
			"Name": "新西兰",
			"IconUrl": "country_flag_newzealand.png"
		},
    cd_southkorea: {
			"CategoryId": "cd_southkorea",
			"Name": "韩国",
			"IconUrl": "country_flag_southkorea.png"
		}
};

export const OTHER_COUNTRY = {
	cd_italy:	{
		"CategoryId": "cd_italy",
		"Name": "意大利",
		"IconUrl": "country_flag_italy.png"
	},
	cd_taiwan:	{
		"CategoryId": "cd_taiwan",
		"Name": "意大利",
		"IconUrl": "country_flag_taiwan.png"
	},
};

export const HOT_PRODUCT = [
	{
      "Symbol": "HuaTong_HuaTong_HTAg",
      "SymbolDisplay": "白银现货排期",
      "Category": "HuaTong_HuaTong",
      "CategoryDisplay": "华通铂银"
    },
    {
      "Symbol": "HuaTong_HuaTong_AgJC",
      "SymbolDisplay": "白银基差1000",
      "Category": "HuaTong_HuaTong",
      "CategoryDisplay": "华通铂银"
    },
    {
      "Symbol": "HuaTong_HuaTong_AgSTS",
      "SymbolDisplay": "白银升贴水1000",
      "Category": "HuaTong_HuaTong",
      "CategoryDisplay": "华通铂银"
    },
    {
      "Symbol": "FX168_SGE_SGBP",
      "SymbolDisplay": "上海金基准价",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AU995",
      "SymbolDisplay": "黄金995",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AU9995",
      "SymbolDisplay": "黄金9995",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AU9999",
      "SymbolDisplay": "黄金9999",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AU100G",
      "SymbolDisplay": "金条100G",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AU50G",
      "SymbolDisplay": "金条50G",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    },
    {
      "Symbol": "FX168_SGE_AUTD",
      "SymbolDisplay": "黄金TD",
      "Category": "FX168_SGE",
      "CategoryDisplay": "上海黄金"
    }
];

//手机号校验器
export function phoneNumberValidator(control: FormControl): any {
  let errorMsg = '',
  flag = true;//true为警告，false为错误。
  if (!control.value) {
    errorMsg = '请输入手机号';
  } else {
    errorMsg = '手机号格式不正确';
  }
  const phoneNumberReg = /^1[34578]\d{9}$/;
  return phoneNumberReg.test(control.value) ? null : { phoneNumber: { desc: errorMsg, flag: flag } };
}

//密码一致校验器
export function passwordMatchValidator(g: FormGroup) {
   return (g.get('PasswordConfirm').value && g.get('Password').value !== g.get('PasswordConfirm').value) ? {'mismatch': true} : null;
}
