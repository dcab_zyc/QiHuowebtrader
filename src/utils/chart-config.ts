import * as moment from 'moment';

export const BASE_OPTION = {
    backgroundColor: '#000',
    animation: true,
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            animation: false,
            lineStyle: {
                color: '#015cad',
                width: 2,
                opacity: 1
            }
        },
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 10,
        textStyle: {
            color: '#fff'
        },
        position: function (pos, params, el, elRect, size) {
            var obj = { top: 10 };
            obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 30;
            return obj;
        },
        extraCssText: 'width: 170px'
    },
    axisPointer: {
        link: { xAxisIndex: 'all' },
        label: {
            backgroundColor: '#777'
        }
    },
    grid: [
        {
            name: 'base',
            left: '6%',
            right: '4%',
            height: '80%'
        }
        // {
        //     left: '10%',
        //     right: '8%',
        //     bottom: '10%',
        //     height: '50'
        // },
        // {
        //     left: '10%',
        //     right: '8%',
        //     bottom: '30%',
        //     height: '50'
        // }
    ],
    xAxis: [{
        type: 'category',
        data: [],
        scale: true,
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#aaa'
            }
        },
        splitLine: { show: false },
        splitNumber: 20,
        min: 'dataMin',
        max: 'dataMax',
        axisPointer: {
            z: 100
        }
    }],
    yAxis: [{
        scale: true,
        splitLine: {
            show: false
        },
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#aaa'
            }
        },
    }],
    dataZoom: [
        {
            type: 'inside',
            xAxisIndex: [0],
            start: 60,
            end: 100
        }
    ],
    series: [
        // {
        //     itemStyle: {
        //         normal: {
        //             borderColor: '#d00202',
        //             borderColor0: '#48b93f'
        //         }
        //     }
        // }
    ]
};

export const XAXIS = {
    type: 'category',
    //data: [],
    scale: true,
    // axisLine: { onZero: false }, // 控制水平线0下
    splitLine: { show: false },
    axisLabel: { show: false },
    splitNumber: 20,
    min: 'dataMin',
    max: 'dataMax',
    axisPointer: {
        z: 100
    }
};

export const YAXIS_BASE = {
    scale: true,
    gridIndex: 1,
    min: "dataMin",
    max: "dataMax",
    axisLabel: { show: false },
    axisLine: { show: false },
    axisTick: { show: false },
    splitLine: { show: false }
};

export const TEST_CHARTDATA = {
    Close: [2, 1, 3, 1, 3, 1, 2, 3, 2, 2],
    High: [3, 2, 3, 4, 3, 3, 3, 2, 3, 5],
    Interval: 60,
    Low: [2, 1, 2, 3, 2, 2, 2, 1, 2, 4],
    Open: [3, 2, 1, 3, 1, 3, 1, 2, 3, 2],
    Symbol: 'FX168_MSFX_GBP',
    Time: [1499910060000, 1499910120000, 1499910180000, 1499910240000, 1499910300000, 1499910360000, 1499910420000, 1499910480000, 1499910540000, 1499910600000],
    Volume: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}

export const TEST_CHARTDATA_EMA_SUCCESS = [2, 1.8182, 2.0331, 1.8453, 2.0552, 1.8633, 1.8882, 2.0903, 2.0739, 2.0605];

export const TEST_CHARTDATA_MA_SUCCESS = [2, 1.5, 2, 1.75, 2, 1.8, 2, 2, 2.2, 2];

export const TEST_CHARTDATA_MACD_SUCCESS = [[0, -0.0797, 0.0182, -0.0648, 0.0304, -0.0549, -0.0413, 0.0496, 0.0405, 0.0329],
[0, -0.0159, -0.0091, -0.0202, -0.0101, -0.0191, -0.0235, -0.0089, 0.001, 0.0074],
[0, -0.0638, 0.0273, -0.0446, 0.0405, -0.0358, -0.0178, 0.0585, 0.0395, 0.0255]];

export const TEST_CHARTDATA_BIAS_SUCCESS = [0, -33.333, 50, -40, 28.571, -40, 0, 50, -14.286, -14.286];

export const TEST_CHARTDATA_BOLL_SUCCESS = [[2, 1.5, 2, 1.6667, 2.3333, 1.6667, 2, 2, 2.3333, 2.3333],
[2, 2.5, 3.633, 3.5523, 4.2189, 3.5523, 3.633, 3.633, 3.2761, 3.2761],
[2, 0.5, 0.367, -0.2189, 0.4477, -0.2189, 0.367, 0.367, 1.3905, 1.3905]];


export const TEST_CHARTDATA_RSI_SUCCESS = [50, 0, 66.6667, 40.0012, 57.1422, 44.4444, 50, 60, 44.4444, 57.1422];

export const TEST_CHARTDATA_ZERO = {
    Close: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    High: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    Interval: 60,
    Low: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    Open: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    Symbol: 'FX168_MSFX_GBP',
    Time: [1499910060000, 1499910120000, 1499910180000, 1499910240000, 1499910300000, 1499910360000, 1499910420000, 1499910480000, 1499910540000, 1499910600000],
    Volume: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}

export const TEST_CHARTDATA_ZERO_ONE = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
export const TEST_CHARTDATA_RSI_ZERO_ONE = [50, 50, 50, 50, 50, 50, 50, 50, 50, 50];
export const TEST_CHARTDATA_ZERO_THREE = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];

export const TEST_CHARTDATA_UNDERZERO = {
    Close: [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
    High: [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
    Interval: 60,
    Low: [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
    Open: [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
    Symbol: 'FX168_MSFX_GBP',
    Time: [1499910060000, 1499910120000, 1499910180000, 1499910240000, 1499910300000, 1499910360000, 1499910420000, 1499910480000, 1499910540000, 1499910600000],
    Volume: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}

export const TEST_CHARTDATA_UNDERZERO_ONE = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
export const TEST_CHARTDATA_UNDERZERO_THREE = [[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]];


export const TEST_CHARTDATA_ONE = {
    Close: [4],
    High: [6],
    Interval: 60,
    Low: [1],
    Open: [3],
    Symbol: 'FX168_MSFX_GBP',
    Time: [1499910060],
    Volume: [0],
}

export const TEST_CHARTDATA_ONE_ONE = [4];
export const TEST_CHARTDATA_ONE_THREE = [[4], [4], [4]];
export const TEST_CHARTDATA_RSI_ONE_ONE = [50];
export const TEST_CHARTDATA_BIAS_ONE_ONE = [0];
export const TEST_CHARTDATA_MACD_ONE_THREE = [[0], [0], [0]];

export const TEST_CHARTDATA_NEW = {
    Close: [2, 1, 3, 1, 3, 1, 2, 3, 2, 2, 3],
    High: [3, 2, 3, 4, 3, 3, 3, 2, 3, 5, 3],
    Interval: 60,
    Low: [2, 1, 2, 3, 2, 2, 2, 1, 2, 4, 2],
    Open: [3, 2, 1, 3, 1, 3, 1, 2, 3, 2, 2],
    Symbol: 'FX168_MSFX_GBP',
    Time: [1499910060000, 1499910120000, 1499910180000, 1499910240000, 1499910300000, 1499910360000, 1499910420000, 1499910480000, 1499910540000, 1499910600000],
    Volume: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}
export const TEST_CHARTDATA_MA_UPDATE_SUCCESS = [2, 1.5, 2, 1.75, 2, 1.8, 2, 2, 2.2, 2, 2.4];
export const TEST_CHARTDATA_BOLL_UPDATE_SUCCESS = [[2, 1.5, 2, 1.6667, 2.3333, 1.6667, 2, 2, 2.3333, 2.3333, 2.3333],
[2, 2.5, 3.633, 3.5523, 4.2189, 3.5523, 3.633, 3.633, 3.2761, 3.2761, 3.2761],
[2, 0.5, 0.367, -0.2189, 0.4477, -0.2189, 0.367, 0.367, 1.3905, 1.3905, 1.3905]];

export const TEST_CHARTDATA_RSI_UPDATE_SUCCESS = [50, 0, 66.6667, 40.0012, 57.1422, 44.4444, 50, 60, 44.4444, 57.1422, 50];

export const TEST_CHARTDATA_BIAS_UPDATE_SUCCESS = [0, -33.333, 50, -40, 28.571, -40, 0, 50, -14.286, -14.286, 28.571];

export const TEST_CHARTDATA_EMA_UPDATE_SUCCESS = [2, 1.8182, 2.0331, 1.8453, 2.0552, 1.8633, 1.8882, 2.0903, 2.0739, 2.0605, 2.2313];

export const TEST_CHARTDATA_MACD_UPDATE_SUCCESS = [[0, -0.0797, 0.0182, -0.0648, 0.0304, -0.0549, -0.0413, 0.0496, 0.0405, 0.0329, 0.1063], [0, -0.0159, -0.0091, -0.0202, -0.0101, -0.0191, -0.0235, -0.0089, 0.001, 0.0074, 0.0272], [0, -0.0638, 0.0273, -0.0446, 0.0405, -0.0358, -0.0178, 0.0585, 0.0395, 0.0255, 0.0791]];
