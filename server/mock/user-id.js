'use strict';

const data = {
  "StatusCode": 3006,
  "Messages": [
    "string"
  ],
  "Data": {
    "User": {
      "Id": "string",
      "UserName": "string",
      "NickName": "string",
      "AvatarUrl": "string",
      "Abstract": "string",
      "Gender": 0,
      "Brands": [
        {
          "Id": 0,
          "ImageUrl": "string"
        }
      ],
      "TradeAdviceCount": 0,
      "FollowingCount": 0,
      "FollowerCount": 0,
      "CreatedTime": 0
    },
    "IsFollowed": true,
    "CreatedTime": 0
  }
};

module.exports = data;