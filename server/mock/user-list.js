'use strict';

const data = {
  "StatusCode": 3006,
  "Messages": [
    "string"
  ],
  "Data": [
    {
      "Id": "1",
      "AccountName": "姓名1",
      "PhoneNumber": "12345678901",
      "PasswordLastModifiedTime": "2017-01-01",
      "NickName": "别名",
      "Gender": 0,
      "AvatarUrl": "asd",
      "Role": {
        "Id": 0,
        "Name": "不知道"
      },
      "IsLocked": false,
      "CreatedTime": 0
    },
    {
      "Id": "1",
      "AccountName": "姓名2",
      "PhoneNumber": "12345678901",
      "PasswordLastModifiedTime": "2017-01-01",
      "NickName": "别名",
      "Gender": 0,
      "AvatarUrl": "asd",
      "Role": {
        "Id": 0,
        "Name": "不知道"
      },
      "IsLocked": true,
      "CreatedTime": 0
    }
  ]
};

module.exports = data;