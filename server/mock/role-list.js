'use strict';

const data = {
  "StatusCode": 3006,
  "Messages": [
    "string"
  ],
  "Data": [
    {
      "Id": 1,
      "Name": "角色1"
    },
    {
      "Id": 2,
      "Name": "角色2"
    }
  ]
};

module.exports = data;