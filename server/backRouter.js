const Mock = require('mockjs');

module.exports = (router, app) => {
  router.post('/login', (ctx, next) => {
      const result = require('./mock/login-post');

      ctx.body = result;
  });

  router.get('/api/Account/UserProfiles', (ctx, next) => {
      const result = require('./mock/user-list');

      ctx.body = result;
  });

  router.get('/api/Account/UserProfile', (ctx, next) => {
    const result = require('./mock/user-id');
    ctx.body = result;
  });

  router.post('/api/Account/Roles', (ctx, next) => {
    const result = require('./mock/role-list');
    ctx.body = result;
  });

  router.post('/api/Account/UpdateUserProfile', (ctx, next) => {
    const result = {
      "StatusCode": 3006,
      "Messages": [
        "string"
      ],
      "Data": {}
    };
    ctx.body = result;
  });

   router.post('/api/Account/Register/ByAdministrator', (ctx, next) => {
    const result = {
      "StatusCode": 3006,
      "Messages": [
        "string"
      ],
      "Data": {}
    };
    ctx.body = result;
  });
        
  router.get('/api/Promote/Promoters', (ctx, next) => {
      const result = require('./mock/advertisers-list');

      ctx.body = result;
  });

  router.get('/api/Promote/Search/ByPromoterId', (ctx, next) => {
      const result = require('./mock/advertisers-search');

      ctx.body = result;
  });

  router.get('/api/quote/GetHistoricalQuote', (ctx, next) => {
      const result = require('./mock/chart-data');

      ctx.body = result;
  });

};