'use strict';

module.exports = {
  'main': '../src/index-prod.ts',
  'polyfill': '../src/polyfill.ts'
};
