/**
 * @author: @AngularClass
 */

//const helpers = require('./helpers');

/**
 * Webpack Plugins
 */

let webpack = require('webpack');
var path = require('path');
const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');

/**
 * Webpack Constants
 */
const ENV = process.env.ENV = process.env.NODE_ENV = 'test';



module.exports = {
  module: require('./module-test'),

  plugins: require('./plugins-test'),

  resolve: require('./resolve'),

  devtool: 'source-map',

  performance: {
    hints: false
  },

  externals: {
    "jquery" : "jQuery",
    'bootstrap': 'bootstrap'
  },
  node: {
    global: true,
    process: false,
    crypto: 'empty',
    module: false,
    clearImmediate: false,
    setImmediate: false
  }
};