'use strict';

let path = require('path');
let webpack = require('webpack');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var CompressionPlugin = require("compression-webpack-plugin");

module.exports = [
  new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)@angular/,
      path.resolve(__dirname, '../src')
    ),
    new HtmlWebpackPlugin({
      title: '测试页面',
      template: 'index.html',
      inject: 'body',
      pushState: true,
      hash: false
    }),
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery'
    }),
    new CleanWebpackPlugin(['temp'], {
      root: path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: [],
      watch: false,
    }),
    new ExtractTextPlugin("styles.css"),
    new CopyWebpackPlugin([{
        from: path.resolve(__dirname, '../src/assets'),
        to: path.resolve(__dirname, '../dist/assets'),
        toType: 'dir'
    }, {
        from: path.resolve(__dirname, '../src/libs'),
        to: path.resolve(__dirname, '../dist/libs'),
        toType: 'dir'
    }]),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
      sassLoader: {
        includePaths: [path.resolve(__dirname, '../src')]
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: true
      },
      output: {
        comments: false
      },
      sourceMap: false
    }),
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.html$/,
        threshold: 10240,
        minRatio: 0.8
    })
];
