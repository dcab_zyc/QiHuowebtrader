'use strict';

let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader',
          'angular-router-loader',
          'angular2-template-loader'
        ],
      },
      {
        test: /\.scss$/,
        //include: [/styles/, /libs\/css/],
        loader: ExtractTextPlugin.extract({fallback:"style-loader",use:["css-loader","sass-loader", "autoprefixer-loader"]})
      },
      // {
      //   test: /\.(css|scss)$/,
      //   loader: ExtractTextPlugin.extract({fallback:"style-loader",use:["css-loader","sass-loader", "autoprefixer-loader"]}),
      //   exclude: [/src\/libs/, /node_modules/, /src\/styles/]
      // },
      {
        test: /\.(jpg|png|gif)$/, 
        use: 'url-loader?limit=1000&name=assets/image/[name].[ext]' 
      },
      { 
        test: /\.(woff|woff2|svg|eot|ttf)$/,
        use: 'url-loader?name=libs/fonts/[name].[ext]'},
      {
        test: /\.html$/,
        loader: 'raw-loader'
      }
    ]
};
