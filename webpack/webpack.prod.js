'use strict';
let path = require('path');

module.exports = {
  entry: require('./entry-prod'),

  context: path.join(process.cwd(), 'src'),

  output: require('./output'),

  module: require('./module'),

  plugins: require('./plugins'),

  resolve: require('./resolve'),

  devServer: require('./dev-server'),

  stats: 'errors-only',

  devtool: 'source-map',
  externals: {
    "jquery" : "jQuery",
  }
};
