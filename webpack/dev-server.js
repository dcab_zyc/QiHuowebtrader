'use strict';

module.exports = {
    contentBase: './src',
    publicPath: '',
    quiet: false,
    noInfo: false,
    host: '0.0.0.0',
    port: 3000,
    stats: {
        chunks: false,
        chunkModules: false,
        modules: false,
        colors: true
    },
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    },
    hot: true,
    historyApiFallback: true,
    lazy: false,
    watchOptions: {
        aggregateTimeout: 300,
        poll: true
    }
};