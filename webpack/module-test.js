'use strict';

let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            query: {
              // use inline sourcemaps for "karma-remap-coverage" reporter
              sourceMap: false,
              inlineSourceMap: true,
              compilerOptions: {

                // Remove TypeScript helpers to be injected
                // below by DefinePlugin
                removeComments: true

              }
            },
          },
          'angular2-template-loader'
        ],
        exclude: [/\.e2e\.ts$/]
      },
      {
        test: /\.scss$/,
        //include: [/styles/, /libs\/css/],
        loader: ExtractTextPlugin.extract({fallback:"style-loader",use:["css-loader","sass-loader"]})
      },
      {
        test: /\.(jpg|png|gif)$/, 
        use: 'url-loader?limit=1000&name=assets/image/[name].[ext]' 
      },
      { 
        test: /\.(woff|woff2|svg|eot|ttf)$/,
        use: 'url-loader?name=libs/fonts/[name].[ext]'},
      {
        test: /\.html$/,
        loader: 'raw-loader'
      },
      {
        enforce: 'post',
        test: /\.(js|ts)$/,
        loader: 'istanbul-instrumenter-loader',
        query: {
            esModules: true
        },
        include: path.resolve(__dirname, '../src'),
        exclude: [
          /\.(e2e|spec)\.ts$/,
          /node_modules/
        ]
      }
    ]
};