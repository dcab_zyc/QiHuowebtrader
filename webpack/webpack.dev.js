'use strict';
let path = require('path');

module.exports = {
    entry: require('./entry-dev'), //入口文件

    context: path.join(process.cwd(), 'src'), //入口路径

    output: require('./output-dev'), //输出路径

    module: require('./module-dev'), //模块配置

    plugins: require('./plugins-dev'), //插件配置

    resolve: require('./resolve'),

    devServer: require('./dev-server'),

    stats: 'errors-only',

    devtool: 'source-map',

    externals: {
        "jquery": "jQuery",
        'bootstrap': 'bootstrap'
    }
};