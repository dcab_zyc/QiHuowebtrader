var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

function root(__path) {
  return path.join(__dirname, __path);
}

module.exports = {
  entry: {
    index: './src/index-dev.ts',
    vendor: './src/polyfill.ts',
  },
  output: {
    path: path.resolve(__dirname, '../temp'),
    filename: '[name].bundle.js',
    chunkFilename: '[id].[chunkhash].bundle.js',
    publicPath: ''
  },
  resolve: {
    extensions: ['.ts', '.js', '.json', '.css', '.html'],
    modules: ["src", "node_modules"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=env',
      },
      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader',
          'angular-router-loader',
          'angular2-template-loader',
        ],
      },
      {
        test: /\.scss$/,
        //include: [/styles/, /libs\/css/],
        loader: ExtractTextPlugin.extract({fallback:"style-loader",use:["css-loader","sass-loader", "autoprefixer-loader"]})
      },
      {
        test: /\.(jpg|png|gif)$/, 
        use: 'url-loader?limit=1000&name=assets/image/[name].[ext]' 
      },
      { 
        test: /\.(woff|woff2|svg|eot|ttf)$/,
        use: 'url-loader?name=libs/fonts/[name].[ext]'},
      {
        test: /\.html$/,
        loader: 'raw-loader'
      }
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)@angular/,
      path.resolve(__dirname, '../src')
    ),
    new HtmlWebpackPlugin({
      title: '测试页面',
      template: 'src/index.html',
      inject: 'body',
      pushState: true,
      hash: false
    }),
    // new webpack.ProvidePlugin({
    //   jQuery: 'jquery',
    //   $: 'jquery',
    //   jquery: 'jquery'
    // }),
    new CleanWebpackPlugin(['temp'], {
      root: path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: [],
      watch: false,
    }),
    new CopyWebpackPlugin([{
        from: path.resolve(__dirname, '../src/assets'),
        to: path.resolve(__dirname, '../temp/assets'),
        toType: 'dir'
    }, {
        from: path.resolve(__dirname, '../src/libs'),
        to: path.resolve(__dirname, '../temp/libs'),
        toType: 'dir'
    }]),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin('style.css'),
    //extractSass
  ],
  externals: {
    //"jquery" : "jQuery",  //不将文件打包,
    //"zone.js": "zone.js",
  },
  devServer:{ 
    contentBase: 'temp',
    publicPath: '',
    quiet: false,
    noInfo: false,
    host: '0.0.0.0',
    port: 3000,
    stats: { 
      chunks: false,
      chunkModules: false,
      modules: false,
      colors: true 
    },
    headers: { 
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept' 
    },
     hot: true,
     historyApiFallback: true,
     lazy: false,
     watchOptions: {
        aggregateTimeout: 300,
        poll: true
    },
  }
};