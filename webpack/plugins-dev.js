'use strict';

let path = require('path');
let webpack = require('webpack');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = [
    new webpack.ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)@angular/,
        path.resolve(__dirname, '../src')
    ),
    new HtmlWebpackPlugin({
        title: '测试页面',
        template: 'index.html',
        inject: 'body',
        pushState: true,
        hash: false
    }),
    new ExtractTextPlugin('style.css'),
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery'
    }),
    new CleanWebpackPlugin(['temp'], {
        root: path.resolve(__dirname, '../'),
        verbose: true,
        dry: false,
        exclude: [],
        watch: false,
    }),
    new CopyWebpackPlugin([{
        from: path.resolve(__dirname, '../src/assets'),
        to: path.resolve(__dirname, '../temp/assets'),
        toType: 'dir'
    }, {
        from: path.resolve(__dirname, '../src/libs'),
        to: path.resolve(__dirname, '../temp/libs'),
        toType: 'dir'
    }]),
    new webpack.HotModuleReplacementPlugin()
];